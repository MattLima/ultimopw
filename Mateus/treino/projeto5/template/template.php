<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css');?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">  
    
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>
</head>

<body>

	<div class="topo-total">

            <div class="menu-total">
	
                <div class="menu">

                    <ul>
                        <li><a href="[url]/" title="">A Loja</a></li>
                        <li><a href="[url]/" title="">Decoração</a></li>
                        <li><a href="[url]/" title="">Jardins Verticais</a></li>
                        <li><a href="[url]/" title="">Natal</a></li>
                    </ul>

                    <div class="logo-menu">
                        <img src="[template]/pw-images/logo-projeto5.png" alt="">
                    </div>

                    <ul>
                        <li><a href="[url]/" title="">Plantas Permanentes</a></li>
                        <li><a href="[url]/" title="">Projetos</a></li>
                        <li><a href="[url]/" title="">Clientes</a></li>
                        <li><a href="[url]/" title="">Contato</a></li>
                    </ul>

                </div> <!-- Menu -->

                
            
            </div> <!-- menu Total -->

            <div class="box-banner">

                <div class="banner-titulo">
                    <h2>Projetos de paisagem</h2>
                </div>
                
            </div>

            <div class="box-banner-sub">

                <div class="banner-subtitulo">
                    <p>de alto padrão personalizado</p>
                </div>
                
            </div>


            <div class="box-banner-02">
                
                <div class="banner-02-conteudo">
                    <h2>Projetos de paisagem</h2>
                    <p>de alto padrão personalizado</p>
                </div>
                
                
            </div>
        
            <div class="imagem-banner">

                <img src="[template]/pw-images/bannerTopo.jpg" alt="">

            </div>
        
    </div> <!-- Topo Total -->
    
    

<div class="global">

	[conteudo]
	
</div> <!-- global -->

	<div class="rodape-total">
        
        <div class="logo-rodape">
            <img src="[template]/pw-images/logo-projeto5.png" alt="">
        </div>

    	<div class="rodape">

            <div class="rodape-conteudo">
                <p>Funcionamento</p>
                <p>Redes Sociais <i class="fab fa-facebook-square"></i> <i class="fab fa-instagram"></i> </p> 
                <p>whatsapp</p>
            </div>

            <div class="rodape-conteudo">
                <p> <i class="fas fa-envelope"></i> contato@artillac.com.br</p>
                <p> <i class="fas fa-phone"></i> (11)3714-8984</p>
                <p> <i class="fas fa-map-marker-alt"></i> Av.Professor-Butanta</p>
            </div>

            <div class="rodape-conteudo">
                <h2>Receba Novidades</h2>
                <h3>Assine nossa newsletter para receber noticias semanais, <br> atualizacoes ofertas especiais e descontos exclusivos</h3>
                <input type="text" class="inpNome" placeholder="Nome">
                <input type="text" class="inpEmail" placeholder="E-mail">
                <button>
                    <i class="fas fa-paper-plane"></i>
                </button>

                    

                
                
            </div>
            
        </div> <!-- Rodape -->
    
    </div> <!-- Rodape Total -->
	
        <script><?php echo file_get_contents('pw-js/jquery.js');?></script>
            <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
            <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
            <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
            <script>
            
                (function($) {	
                    'use strict';
                    window.sr = ScrollReveal();
                    sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
                })();
                
                
        </script>



</body>

</html>