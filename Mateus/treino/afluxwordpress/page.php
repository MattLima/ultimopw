<?php get_header(); ?>

	<div class="conteudo-pages">

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="titulo-total">
							<h1><?php echo get_the_title(); ?></h1>
						</div>
						<div class="texto">
							<p><?php echo the_content(); ?></p>
							
						</div> <!-- Texto -->

					<?php endwhile; ?>
					<?php endif; ?>

	</div> <!-- conteudo-pages -->


<?php get_footer(); ?>