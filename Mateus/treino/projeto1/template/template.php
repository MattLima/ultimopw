<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />[meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    [css]
    <style><?php echo file_get_contents('template/pw-css/style.css');?></style>
    <style><?php echo file_get_contents('template/pw-slider-engine/style.css');?></style>
    <style><?php echo file_get_contents('pw-font-awesome/css/all.css');?></style>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    
</head>

<body>

    <div class="topo-total">

        <div class="linha-topo-total">

            <div class="linha-topo">

                <div class="dados">

                    <div class="item">

                        <div class="img"><i class="fas fa-phone"></i></div>
                        <!-- img -->

                        <div class="texto">
                            (19) 3826-2294 / 98220-7552
                        </div>
                        <!-- texto -->

                    </div>
                    <!-- item -->

                    <div class="item">

                        <div class="img"><i class="fas fa-envelope"></i></div>
                        <!-- img -->

                        <div class="texto">
                            comercial@projetoweb.com.br
                        </div>
                        <!-- texto -->

                    </div>
                    <!-- item -->

                </div>
                <!-- dados -->

                <div class="redes">

                    <div class="img"><a href=""><i class="fab fa-facebook-f"></i></a></div>
                    <div class="img"><a href=""><i class="fab fa-instagram"></i></a></div>

                </div>
                <!-- redes -->

            </div>
            <!--linha topo -->

        </div>
        <!-- linha topo total -->

        <div class="topo">

            <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="" title="" /></a></div>
            <!-- Logo -->

            <div class="menu-total">

                <div class="menu-resp"></div>
                <!-- Menu Resp -->

                <div class="menu">

                    <ul>
                        <li><a href="[url]/" title="Página inicial">HOME</a></li>
                        <li><a href="[url]/empresa-desenvolve-sites" title="Empresa">EMPRESA</a></li>
                        <li><a href="[url]/otimização-sites" title="Otimização">SERVIÇOS</a></li>
                        <li><a href="[url]/modelos-sites" title="Portfolio">PORTFÓLIO</a></li>
                        <li><a href="[url]/contato-site" title="Contato">CONTATO</a></li>
                    </ul>

                </div>
                <!-- Menu -->

            </div>
            <!-- menu Total -->

        </div>
        <!-- Topo -->

    </div>
    <!-- Topo Total -->

    <div class="global">

        [conteudo]

    </div>
    <!-- global -->

    <div class="rodape-total">

        <div class="rodape">

            <div class="texto-rodape">

                <div class="esquerda">

                    <div class="img"><i class="fas fa-phone"></i></div>
                    <!-- img -->

                    <div class="texto">
                        (19) 3826-2294 / 98220-7552
                    </div>
                    <!-- texto -->

                </div>
                <!-- esquerda -->

                <div class="direita">

                    <div class="img"><i class="fas fa-envelope"></i></div>
                    <!-- img -->

                    <div class="texto">
                        comercial@projetoweb.com.br
                    </div>
                    <!-- texto -->

                </div>
                <!-- direita -->

            </div>
            <!-- Texto Rodape -->

            <div class="redes-rodape">
                <img src="[template]/pw-images/logo-facebook.png" alt="" />
                <img src="[template]/pw-images/logo-instagram.png" alt="" />
            </div>
            <!-- Redes Rodape -->

        </div>
        <!-- Rodape -->

            <div class="logo-pw">
                <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
                <div>
                    <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                    <p>
                        <a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
                        <a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites/</span></a>
                        <a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site/</span></a>
                        <a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site/</span></a>
                        <a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites/</span></a>
                        <a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas/</span></a>
                        <a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
                    </p>
                </div>
            </div>

        <div class="faixa-rodape" style="display: none;">
            <?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php');?>
        </div>
    </div>
    <!-- Rodape Total -->

    <script><?php echo file_get_contents('pw-js/jquery.js');?></script>
    <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
    <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
    <script><?php echo file_get_contents('template/pw-slider-engine/wowslider.js');?></script>
    <script><?php echo file_get_contents('template/pw-slider-engine/script.js');?></script>
    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.logo', {
                duration: 2000,
                origin: 'left',
                distance: '100px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.menu', {
                duration: 2000,
                origin: 'left',
                distance: '300px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('h1', {
                duration: 2000,
                origin: 'left',
                distance: '300px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            
            sr.reveal('.servico', {
                duration: 2000,
                origin: 'bottom',
                distance: '10px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-topo', {
                duration: 2000,
                origin: 'top',
                distance: '20px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.banner .texto', {
                duration: 2000,
                origin: 'bottom',
                distance: '20px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-01', {
                duration: 2000,
                origin: 'bottom',
                distance: '100px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-02 .conteudo.centro', {
                duration: 2000,
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-02 .conteudo.direita', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-02 .conteudo.esquerda', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.linha-03', {
                duration: 2000,
                origin: 'top',
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.box-02 .item', {
                duration: 2000
            }, 150);
            sr.reveal('.form-esquerda', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.form-direita', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                mobile: false,
                viewFactor: 0.6
            }, 100);
            sr.reveal('.botao', {
                duration: 2000,
                origin: 'bottom',
                distance: '10px',
                mobile: false,
                viewFactor: 0.21
            }, 100);
            sr.reveal('.mapa', {
                duration: 2000,
                origin: 'light',
                distance: '10px',
                mobile: false,
                viewFactor: 0.21
            }, 100);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }
    </script>
</body>

</html>