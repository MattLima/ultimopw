<?php get_header(); ?>

<div id="conteudo-interna">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="titulo-total">
		<?php echo get_the_title(); ?>
	</div>
	<div class="texto">
    	<?php echo the_content(); ?>
	</div> <!-- Texto -->

<?php endwhile; ?>
<?php endif; ?>

</div> <!-- Conteudo-interno -->


<?php get_footer(); ?>