<?php
	$dadosPagina["titulo"]   = "Marmoraria – ExpandGran Mármores e Granitos";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Marmoraria, Granito, Marmore, Escultura, Granito Branco, Marmore Branco, Granito Preto, Pia de Granito Cozinha, Granito cinza, Pia de Marmore, Pia de Granito\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Marmoraria – ExpandGran Mármores e Granitos\" />";
    $dadosPagina["css"] = "<style></style>";
?>
<div class="swiper-container">
	<div class="swiper-wrapper">
		<div class="swiper-slide"><img src="[template]/pw-images/banners/mesa-marmore.jpg" alt="" title="" /></div>
		<div class="swiper-slide"><img src="[template]/pw-images/banners/pia-marmores-granito.jpg" alt="" title="" /></div>
	</div>
	<!-- Add Arrows -->
	<div class="swiper-button-next swiper-button-white"></div>
	<div class="swiper-button-prev swiper-button-white"></div>

</div>
<!-- Swiper -->
<div class="slogan-total">
	<div class="slogan">
		<div class="titulo">Expand Gran</div> <!-- titulo -->
		<div class="texto"><b>Mármores e Granitos</b></div> <!-- texto -->
	</div> <!-- slogan -->
</div>

<div class="box-01-total">

	<div class="box-01">

		<div class="titulo">ENTRE EM CONTATO CONOSCO E CONHEÇA A NOSSA FORMA DE TRABALHO</div>

	</div> <!-- box 01 -->

</div> <!-- box 01 total -->


<div class="box-02-total">

	<div class="box-02">

		<div class="item">
			<a href="">
				<div class="img"><img src="[template]/pw-images/ilha-granito.jpg" alt=""></div> <!-- img -->

				<div class="text">
					<div class="titulo">MARMÓRES</div> <!-- titulo -->
					<div class="texto"></div>
				</div> <!-- texto -->
			</a>
		</div> <!-- item -->

		<div class="item">
			<a href="">
				<div class="img"><img src="[template]/pw-images/mesa-central-granito.jpg" alt=""></div> <!-- img -->
				<div class="text">
					<div class="titulo">GRANITOS</div> <!-- titulo -->
					<div class="texto"></div>
				</div> <!-- texto -->
			</a>
		</div> <!-- item -->

		<div class="item">
			<a href="">
				<div class="img"><img src="[template]/pw-images/orcamento-granito.jpg" alt=""></div> <!-- img -->
				<div class="text">
					<div class="titulo">ORÇAMENTO</div> <!-- titulo -->
					<div class="texto"></div>
				</div> <!-- texto -->
			</a>
		</div> <!-- item -->



	</div> <!-- box 02 -->

</div> <!-- box 02 total -->


<div class="box-03-total">

	<div class="box-03">

		<div class="titulo">COMO FUNCIONA</div> <!-- titulo -->

		<div class="descricao"></div> <!-- descricao -->

		<div class="conteudo">

			<div class="item">
				<div class="numero">1</div>
				<div class="texto">“Quando iniciamos a vida, cada um de nós recebe um bloco de mármore e as ferramentas
					necessárias para convertê-lo em escultura. Podemos arrastá-lo intacto a vida toda, podemos reduzi-lo a cascalho ou podemos dar-lhe uma forma
					gloriosa.” (Richard Bach)
				</div>
			</div> <!-- item -->

			<div class="item">
				<div class="numero">2</div>
				<div class="texto">"Escreve as ofensas na areia e os benefícios no mármore." (Benjamim Franklin)</div>
			</div> <!-- item -->

			<div class="item">
				<div class="numero">3</div>
				<div class="texto">"A educação é para a alma o que a escultura é para um bloco de mármore." (Joseph Addison)</div>
			</div> <!-- item -->

		</div> <!-- conteudo -->

	</div> <!-- box 03 -->

</div> <!-- box 03 total -->


<div class="box-04-total">

	<div class="box-04">

		<div class="titulo">CONTATO</div>


		<form action="pw-form.php" method="post">

			<div class="cima">
				<input name="campo[Nome]" placeholder="Nome:" type="text" />
				<input name="campo[E-mail]" placeholder="E-mail:" type="text">
				<input name="campo[Telefone]" placeholder="Telefone:" type="text">
			</div> <!-- cima -->
			<textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>

			<span><input class="submit" value="Enviar" type="submit" /></span>

		</form>

	</div> <!-- box 04 -->

</div> <!-- box 04 total -->


		<div class="mapa">

			<div class="mapa-img">
				<a href="https://goo.gl/maps/MNPiEMPqkwUoBDCx8"> <img src="[template]/pw-images/mapa-Expand-Gran.jpg" alt=" ExpandGran Localização"></a>
			</div>

			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.3818239612383!2d-46.79496574920205!3d-23.518765584629048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cefe50e6461347%3A0xeeaf3d69d992d3e3!2sR.%20Amador%20Bueno%2C%20490%20-%20Piratininga%2C%20Osasco%20-%20SP%2C%2006230-100!5e0!3m2!1spt-BR!2sbr!4v1620386609556!5m2!1spt-BR!2sbr" width="1920" height="300" style="border:0" allowfullscreen>
			</iframe>

		</div> <!-- mapa -->