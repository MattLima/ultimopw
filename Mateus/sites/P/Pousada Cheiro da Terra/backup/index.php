<?php
	$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>

	<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
		<li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0"/></li>
		<li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1"/></li>
	</ul></div>

	<div class="ws_shadow"></div>
	</div>	
	<!-- End WOWSlider.com BODY section -->


    <div class="box-01-total">

        <a class="entrar" href="" title="" data-icon="empresa">
            <div class="box-01">

                <div class="conteudo">
                    <div class="titulo">BEM-VINDO AO<br />NOSSO SITE</div> <!-- Titulo -->
                    <div class="linha">
                        <div class="barra"></div>
                    </div> <!-- Linha -->
                    <div class="texto">
                       Um grupo de investidores Hispano – Brasileiro, especializado em investimentos de pequenas e medias empresas, envolvido no ramo de Logística e Imobiliário, decidiu concentrar investimentos na Europa e nesse caso mais especificamente em Portugal na área de Logística, trading e Negócios Internacionais.

                    </div> <!-- Texto -->
                    <div class="entrar">Saiba mais</div> <!-- Entrar -->
                </div> <!-- Conteudo -->
                <div class="img">
                    <img src="[template]/pw-images/box-01.jpg" alt="" title="" />
                    <div class="info"><i class="far fa-thumbs-up"></i><br />"Atender Bem Para Atender Sempre"</div> <!-- Info -->
                </div> <!-- Img -->

            </div> <!-- Box 01 -->
        </a>

    </div> <!-- conteudo index -->


    <div class="box-02-total">

        <div class="box-02">

            <a class="conteudo" href="" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-01.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">LOGÍSTICA AÉREO</div> <!-- Titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            <a class="conteudo" href="" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-02.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">LOGÍSTICA MARÍTIMO</div> <!-- Titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            <a class="conteudo" href="" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-04.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">LOGÍSTICA RODOVIÁRIO</div> <!-- Titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            <a class="conteudo" href="" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-05.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">DESPACHO ADUANEIRO </div> <!-- Titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            
            

        </div> <!-- BOx 02 -->

    </div> <!-- Box 02 Total -->

    <div class="box-03-total">

        <div class="box-03">

            <div class="linha">
                <div class="barra"></div>
            </div> <!-- Linha -->
            <div class="titulo">ENTRE EM <br />CONTATO</div> <!-- Titulo -->

            <form action="mail-contato.php" method="post">
                <div class="item">
                    <input name="campo[Nome]" type="text" placeholder="Nome:" />
                    <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                    <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
                </div> <!-- item -->
                <div class="item">
                    <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
                </div> <!-- item -->
                <input class="submit" type="submit" value="Enviar" />
            </form>

        </div> <!-- Box 03 -->

    </div> <!-- Conteudo Pages -->

    <div class="mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3003.102862024031!2d-8.685450584334195!3d41.17592361703992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd246f3f9ed5e827%3A0xb30cec4c7c1f2e2c!2sAv.%20Comendador%20Ferreira%20de%20Matos%20682%2C%204450-013%20Matosinhos%2C%20Portugal!5e0!3m2!1spt-BR!2sus!4v1602594637533!5m2!1spt-BR!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

    </div> <!-- mapa -->
