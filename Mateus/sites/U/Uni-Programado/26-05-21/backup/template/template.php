<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css');?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">
    <link rel="stylesheet" href="pw-galeria-fancybox/jquery.fancybox.min.css">
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }

    </style>
</head>

<body>

     <div class="linha-total">

        <div class="linha">
            <div class="item"><i class="fas fa-phone-square"></i> (11) 9.8149-5225</div>
            
            <div class="item"><i class="fas fa-envelope-open-text"></i> faleconosco@equipeuni.com.br</div>
            
            <div class="item"><a target="_blank" href="https://api.whatsapp.com/send?phone=#5511981495225" title="Integrasense - WhatsApp"><i class="fab fa-whatsapp"></i> </a> Whatsapp</div>
            
             <div class="item"><a target="_blank" href="#https://www.youtube.com/channel/UCghTNJxdVJPBJfATRCdoKPw" title="Integrasense - YouTube"><i class="fab fa-facebook-f"></i></a> @Uni</div>
             
            <div class="item"><a target="_blank" href="#https://www.instagram.com/to_integrasense/" title="Integrasense - Instagram"><i class="fab fa-instagram"></i> </a>@Uni</div>
           
        </div>

    </div>   

    <div class="topo-total">

        <div class="topo">

            <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="" title="" /></a></div>

            <div class="menu">

                <ul>
                    <li><a href="[url]/" title="">HOME</a></li> 
                    <li><a href="[url]/empresa" title="">SOBRE A UNI </a></li>
                    <li><a href="[url]/especialidades" title="">ESPECIALIDADES</a></li>
                    <li><a href="[url]/galeria" title="">GALERIA</a></li>
                    <li><a href="[url]/nosso-espaco" title="">NOSSO ESPAÇO</a></li>
                     <li><a href="[url]/noticias" title="">NOTÍCIAS</a></li>
                      <li><a href="[url]/contato" title="">CONTATO</a></li>
                </ul>

            </div>

        </div>

    </div>

    <div class="global">

        [conteudo]

    </div>

    <div class="rodape-total">

        <div class="item-rodape-total">
            <div class="rodape">
                
                <div class="item"><a href="[url]/" title=""><img src="[template]/pw-images/logo-rodape.png" alt="" title="" /></a></div>

            </div>
            
            <div class="rodape contatos">
                <p><strong>Contato</strong></p>
                <div class="item-contato">
                <div class="item"><i class="fas fa-phone-volume"></i> (19) 98424-4728</div>
                
                <div class="item"><i class="fas fa-envelope-open-text"></i> faleconosco@equipeuni.com.br  </div>
                
                <div class="item"><i class="fas fa-map-marker-alt"></i> Avenida Marechal Rondon, 700, 1º andar,  salas 109 a 118 CEP: 13070-172</div>
                
                </div>
            </div>
            <div class="rodape chat">
                <a target="_blank" href="https://api.whatsapp.com/send?phone=5519">
                    <div class="item whats"><i class="fab fa-whatsapp"></i> Atendimento pelo Whatsapp</div>
                </a>                
            </div>
        </div>
        
        <div class="logo-pw-total">
            <div class="logo-pw">
                <div class="item-logo-pw">
                    <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
                    <div>
                        <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                        <p>
                            <a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
                            <a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites</span></a>
                            <a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site</span></a>
                            <a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site</span></a>
                            <a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites</span></a>
                            <a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas</span></a>
                            <a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="faixa-rodape" style="display: none;">
            <?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php ');?>
        </div>

    </div>

    <script>
        <?php echo file_get_contents('pw-js/jquery.js'); ?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/scrollReveal.js'); ?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/javascript.js'); ?>

    </script>
    <script src="[template]/pw-slider-engine/wowslider.js"></script>
    <script src="[template]/pw-slider-engine/script.js"></script>
    <script src="pw-galeria-fancybox/jquery.fancybox.min.js"></script>
    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.box-02 .item', {
                duration: 2000,
                origin: 'right',
                distance: '100px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
            sr.reveal('.box-03 input, .box-03 textarea', {
                duration: 2000,
                origin: 'bottom',
                distance: '50px',
                viewFactor: 0.6,
                mobile: false
            }, 100);
            sr.reveal('.mapa', {
                duration: 2000,
                origin: 'bottom',
                distance: '0px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }

    </script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
        }
    </script>
</body>

</html>
