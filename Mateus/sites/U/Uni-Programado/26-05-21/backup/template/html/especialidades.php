<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>

<div class="conteudo-pages">
    <h1>ESPECIALIDADES</h1>

    <div class="boxPage">
        <div class="col flex">
            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/adriana-psicologa.jpg" alt="" title="">
                </div>
                <h2>Adriana</h2>
                <p><strong>Especialidade: </strong>Psicóloga</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>
            
            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/aline-to.jpeg" alt="" title="">
                </div>
                <h2>Aline</h2>
                <p><strong>Especialidade: </strong>TO</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/amanda-terapeuta-ocupacional.jpg" alt="" title="">
                </div>
                <h2>Amanda</h2>
                <p><strong>Especialidade: </strong>Terapêuta Ocupacional</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/amarilis-fono.jpeg" alt="" title="">
                </div>
                <h2>Amarilis</h2>
                <p><strong>Especialidade: </strong>Fono</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/anayara-psico.jpeg" alt="" title="">
                </div>
                <h2>Anayara</h2>
                <p><strong>Especialidade: </strong>Psico</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/andressa-fisio.jpeg" alt="" title="">
                </div>
                <h2>Andressa</h2>
                <p><strong>Especialidade: </strong>Fisio</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/bianca-pedagoga.jpeg" alt="" title="">
                </div>
                <h2>Bianca</h2>
                <p><strong>Especialidade: </strong>Pedagoga</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/carolina-terapeuta-ocupacional.jpg" alt="" title="">
                </div>
                <h2>Carolina</h2>
                <p><strong>Especialidade: </strong>Terapeuta Ocupacional</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/kharina-fonoaudiologa.jpg" alt="" title="">
                </div>
                <h2>Kharina</h2>
                <p><strong>Especialidade: </strong>Fonoaudiologa</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/michele-to.jpeg" alt="" title="">
                </div>
                <h2>Michele</h2>
                <p><strong>Especialidade: </strong>TO</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>

            <div class="itemBox">
                <div class="imgitem">
                    <img src="[template]/pw-images/especialidades/nayara-fisio.jpeg" alt="" title="">
                </div>
                <h2>Nayara</h2>
                <p><strong>Especialidade: </strong>Fisio</p>
                <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
            </div>
        </div>
    </div>

</div>