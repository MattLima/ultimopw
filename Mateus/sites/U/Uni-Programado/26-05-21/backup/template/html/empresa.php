<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>

<div class="conteudo-pages">
    <h1>SOBRE A UNI</h1>

    <div class="boxPage">
        <h2>Sobre a Uni</h2>

        <div class="col dup">
            <div class="txtBox">
                <p>Há quatro anos, cinco amigas, Adriana, Amanda, Carolina, Kharina e Thatiana, se viram diante de um sonho em comum, construir um espaço de tratamento interdisciplinar que atendesse com a competência aprendida nos anos de formação e trabalhos junto a outras equipes, mas que também contemplasse interação, acolhimento e cuidado especial a cada família que nos procurasse, desenvolvendo um projeto único e coeso junto a esses. </p>
            </div>
            <div class="imgBox">
                <img src="[template]/pw-images/1.jpg" alt="" title="">
            </div>
        </div>

        <div class="col dup">
            <div class="imgBox">
                <img src="[template]/pw-images/2.jpeg" alt="" title="">
            </div>
            <div class="txtBox">
                <p>Muitas reuniões, reflexões, ansiedades e expectativas foram criadas para que essa semente germinasse e pudesse gerar frutos. E assim nasceu esse espaço de personalidades diversas, mas com um sonho em comum que as Uni a cada dia. Um ano depois, mais duas integrantes vieram acrescentar força, determinação e realimentar nossos sonhos, nossas queridas Amarílis e Andressa.</p>
            </div>
        </div>

        <div class="col dup">
            <div class="txtBox">
                <p>Hoje, passados quatro anos, essa equipe se consolida e floresce, inclusive somos atualmente em 12 mulheres, certamente capazes e dispostas a todos os dias dar o seu melhor para contribuir aos tratamentos e a essa grande família chamada Equipe Uni. </p>
            </div>
            <div class="imgBox size">
                <img src="[template]/pw-images/3.jpeg" alt="" title="">
            </div>
        </div>

    </div>

    <div class="mvv-total">
        <div class="mvv">
            <h2>O que éramos, o que somos e para onde almejamos chegar? </h2>

            <div class="item ">
                <h3><i class="fas fa-arrow-circle-right"></i> Missão</h3>
                <p>Prezar pelo acolhimento família-paciente e pelo desenvolvimento global (motor, emocional e capacidade de aprendizagem) do nosso paciente. Conquistados através da continua busca pela Competência, Técnica e Escuta acolhedora e empática. </p>
            </div>

            <div class="item">
                <h3><i class="fas fa-arrow-circle-right"></i> Visão</h3>
                <p>Ser amplamente reconhecida pela excelência e competência técnica UNIdas a uma escuta acolhedora e centrada no bem-estar da família e do paciente.</p>
            </div>

            <div class="item">
                <h3><i class="fas fa-arrow-circle-right"></i> Valores</h3>
                <p>Oportunidade de qualidade de vida através do brincar e da alegria, com a certeza de que TODOS tem potencialidades únicas e que são capazes de se desenvolverem. </p>
                <ul>
                    <li> Ética
                        <p>Atuar e aplicar de maneira responsável a técnica exercida pelo profissional, proporcionando ambiente seguro e acolhedor, tendo como base uma conduta ética precedida pelos valores sociais e as próprias normativas internas, resultando no respeito à necessidade de cada paciente e familiar;</p>
                    </li>
                    <li> Humanização e Acolhimento
                        <p>Oferecer espaço de escuta e cuidado ao sujeito e toda a sua rede de apoio, baseado na concepção da cultura da excelência por meio da empatia, sensibilidade e competência técnica;</p>
                    </li>
                    <li> Excelência
                        <p>Buscar aprimorar-se constantemente nas mais atuais e inovadoras técnicas e abordagens em prol do desenvolvimento do paciente;</p>
                    </li>
                    <li> Desenvolvimento Humano
                        <p>Promover a valorização pessoal e profissional de cada colaborador, baseado no conceito de que favorecer as potencialidades humanas gera crescimento.</p>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="boxPage">
        <h2>TEXTOS DEFINIÇÃO PROFISSÕES  </h2>

        <div class="acordeao">
            <div class="acordeaoItem">
                <button class="accordion blue">TO </button>
                <div class="panel">
                    <p>A Terapia Ocupacional é a profissão que atua na prevenção e na (re)habilitação do sujeito que, por alterações em seus componentes de desempenho ocupacional, isto é, alterações cognitivas, motoras, sensoriais ou afetivo/sociais deixa de executar seus papéis, funções e/ou ocupações do dia a dia (atividades de vida diária: comer, vestir-se, escovar os dentes,  atividades instrumentais de vida diária: preparar lanches, usar Uber, administrar o dinheiro; lazer, educação, brincar e trabalho) interferindo negativamente em sua rotina e performance pessoal. Em resumo, o TO favorece com que o indivíduo realize, de forma mais independente e autônoma possível, as suas atividades cotidianas, como trabalhar, estudar, brincar, usar talheres, etc.</p>
                </div>
            </div>

            <div class="acordeaoItem">
                <button class="accordion green">PSICOLOGIA </button>
                <div class="panel">
                    <p>É a área da ciência dedicada ao estudo da mente e do comportamento humano. Atua no diagnóstico, prevenção e intervenção de acordo com cada caso, considerando a individualidade e a Psicopatologia. O principal objetivo da Psicologia é promover a qualidade de vida do ser humano em sua integridade, considerando aspectos biológicos, psicológicos e sociais.</p>
                </div>
            </div>

            <div class="acordeaoItem">
                <button class="accordion blue">FONOAUDIOLOGIA </button>
                <div class="panel">
                    <p>O fonoaudiólogo é um profissional de saúde, com graduação plena em Fonoaudiologia, que atua de forma autônoma e independente nos setores público e privado. É responsável pela promoção da saúde, prevenção, avaliação e diagnóstico, orientação, terapia (habilitação e reabilitação) e aperfeiçoamento dos aspectos fonoaudiológicos da função auditiva periférica e central, da função vestibular, da linguagem oral e escrita, da voz, da fluência, da articulação da fala e dos sistemas miofuncional, orofacial, cervical e de deglutição. Exerce também atividades de ensino, pesquisa e administrativas. É atuante em unidades básicas de saúde, ambulatórios de especialidades, hospitais e maternidades, consultórios, clínicas, home care, domicílios, asilos e casas de saúde, creches e berçários, escolas regulares e especiais, instituições de ensino superior, empresas, veículos de comunicação (rádio, TV e teatro) e associações. Compõem-se por 14 especialidades, sendo elas: Audiologia, Linguagem, Motricidade Oral, Saúde Coletiva, Voz, Disfagia, Fonoaudiologia Educacional, Gerontologia, Fonoaudiologia Neurofuncional, Fonoaudiologia do Trabalho, Neuropsicologia, Fluência, Perícia Fonoaudiológica, Fonoaudiologia Hospitalar.</p>
                </div>
            </div>

            <div class="acordeaoItem">
                <button class="accordion green">PEDAGOGIA </button>
                <div class="panel">
                    <p>A pedagogia é a ciência que tem como o objeto de estudo a educação, e o processo de ensino e aprendizagem. O pedagogo busca entender o que impede o aluno de aprender e busca entender as influências emocionais, sociais e afetivas que podem favorecer ou desfavorecer a aprendizagem do aluno. O trabalho do pedagogo, pode acontecer no espaço clínico e dentro do ambiente escolar, como em salas de aulas da educação infantil ao ensino fundamental I e professor de AEE (atendimento educacional especializado), e orientador escolar. O que esperar do trabalho do Pedagogo? Orientação de estudos: Auxilia na organização da vida escolar da criança, promover o melhor uso do tempo, a elaboração de uma agenda e organização de materiais necessários para o estudo. </p>
                    <p>Apropriação dos conteúdos escolares: Adaptação dos materiais didáticos e atividades escolares. Proporcionando um domínio maior das atividades que a criança apresenta dificuldades.</p>
                    <p>Desenvolvimento do raciocínio: Treinamento dos processos e pensamentos necessários para a aprendizagem. Jogos são muito utilizados, pois através dos jogos a criança vai construindo a sua forma de aprender.</p>
                    <p>Atendimento de crianças: A pedagoga atende e acompanha a criança com dificuldades durante o seu processo de ensino e aprendizagem.</p>
                </div>
            </div>
        </div>
    </div>

</div>