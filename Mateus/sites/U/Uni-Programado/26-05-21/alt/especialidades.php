<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>

<div class="conteudo-pages">

    <h1>ESPECIALIDADES</h1>

    <div class="boxPage">

        <!-- Control buttons -->
        <div id="myBtnContainer">
            <button class="btn active" onclick="filterSelection('terapia-ocupacional')"> Terapia Ocupacional </button>
            <button class="btn" onclick="filterSelection('psicologia')"> Psicologia</button>
            <button class="btn" onclick="filterSelection('fonoaudiologia')"> Fonoaudiologia</button>
            <button class="btn" onclick="filterSelection('pedagogia')"> Pedagogia</button>
            <button class="btn" onclick="filterSelection('fisioterapia')"> Fisioterapia</button>
        </div>

        <!-- The filterable elements. Note that some have multiple class names (this can be used if they belong to multiple categories) -->
        <div class="container">
            

            <div class="filterDiv terapia-ocupacional">

                <h2>Terapia Ocupacional (TO)</h2>

                <div class="conteudo-filterDiv">


                    <div class="texto-filterDiv">
                        <p> A Terapia Ocupacional é a profissão que atua na prevenção e na (re)habilitação do sujeito que, por alterações em seus componentes de desempenho ocupacional, isto é, alterações cognitivas, motoras, sensoriais ou afetivo/sociais deixa de executar seus papéis, funções e/ou ocupações do dia a dia (atividades de vida diária: comer, vestir-se, escovar os dentes, atividades instrumentais de vida diária: preparar lanches, usar Uber, administrar o dinheiro; lazer, educação, brincar e trabalho) interferindo negativamente em sua rotina e performance pessoal. Em resumo, o TO favorece com que o indivíduo realize, de forma mais independente e autônoma possível, as suas atividades cotidianas, como trabalhar, estudar, brincar, usar talheres, etc.</p>
                    </div>

                    <div class="img-filterDiv">
                        <img src="[template]/pw-images/medicos.png" alt="" title="">
                    </div>

                </div>

            </div>

            <div class="filterDiv psicologia">
                <h2>Psicologia</h2>
                <div class="conteudo-filterDiv">
                    <div class="texto-filterDiv">

                        <p> É a área da ciência dedicada ao estudo da mente e do comportamento humano. Atua no diagnóstico, prevenção e intervenção de acordo com cada caso, considerando a individualidade e a Psicopatologia. O principal objetivo da Psicologia é promover a qualidade de vida do ser humano em sua integridade, considerando aspectos biológicos, psicológicos e sociais.</p>
                    </div>

                    <div class="img-filterDiv">
                        <img src="[template]/pw-images/medicos.png" alt="" title="">
                    </div>

                </div>
            </div>

            <div class="filterDiv fonoaudiologia">

                <h2>Fonoaudiologia</h2>

                <div class="conteudo-filterDiv">

                    <div class="texto-filterDiv">

                        <p> O fonoaudiólogo é um profissional de saúde, com graduação plena em Fonoaudiologia, que atua de forma autônoma e independente nos setores público e privado. É responsável pela promoção da saúde, prevenção, avaliação e diagnóstico, orientação, terapia (habilitação e reabilitação) e aperfeiçoamento dos aspectos fonoaudiológicos da função auditiva periférica e central, da função vestibular, da linguagem oral e escrita, da voz, da fluência, da articulação da fala e dos sistemas miofuncional, orofacial, cervical e de deglutição. Exerce também atividades de ensino, pesquisa e administrativas. É atuante em unidades básicas de saúde, ambulatórios de especialidades, hospitais e maternidades, consultórios, clínicas, home care, domicílios, asilos e casas de saúde, creches e berçários, escolas regulares e especiais, instituições de ensino superior, empresas, veículos de comunicação (rádio, TV e teatro) e associações. Compõem-se por 14 especialidades, sendo elas: Audiologia, Linguagem, Motricidade Oral, Saúde Coletiva, Voz, Disfagia, Fonoaudiologia Educacional, Gerontologia, Fonoaudiologia Neurofuncional, Fonoaudiologia do Trabalho, Neuropsicologia, Fluência, Perícia Fonoaudiológica, Fonoaudiologia Hospitalar.</p>
                    </div>

                    <div class="img-filterDiv">
                        <img src="[template]/pw-images/medicos.png" alt="" title="">
                    </div>

                </div>
            </div>

            <div class="filterDiv pedagogia">
                <h2>Pedagogia</h2>

                <div class="conteudo-filterDiv">

                    <div class="texto-filterDiv">

                        <p> A pedagogia é a ciência que tem como o objeto de estudo a educação, e o processo de ensino e aprendizagem. O pedagogo busca entender o que impede o aluno de aprender e busca entender as influências emocionais, sociais e afetivas que podem favorecer ou desfavorecer a aprendizagem do aluno. O trabalho do pedagogo, pode acontecer no espaço clínico e dentro do ambiente escolar, como em salas de aulas da educação infantil ao ensino fundamental I e professor de AEE (atendimento educacional especializado), e orientador escolar. O que esperar do trabalho do Pedagogo?

                            <ul>
                                <li>Orientação de estudos: Auxilia na organização da vida escolar da criança, promover o melhor uso do tempo, a elaboração de uma agenda e organização de materiais necessários para o estudo. </li>
                                <li>Apropriação dos conteúdos escolares: Adaptação dos materiais didáticos e atividades escolares. Proporcionando um domínio maior das atividades que a criança apresenta dificuldades;</li>
                                <li>Desenvolvimento do raciocínio: Treinamento dos processos e pensamentos necessários para a aprendizagem. Jogos são muito utilizados, pois através dos jogos a criança vai construindo a sua forma de aprender;</li>
                                <li>Atendimento de crianças: A pedagoga atende e acompanha a criança com dificuldades durante o seu processo de ensino e aprendizagem.</li>
                            </ul>
                        </p>
                    </div>

                    <div class="img-filterDiv">
                        <img src="[template]/pw-images/medicos.png" alt="" title="">
                    </div>

                </div>
            </div>

            <div class="filterDiv fisioterapia">
                <h2>Fisioterapia</h2>

                <div class="conteudo-filterDiv">

                    <div class="texto-filterDiv">

                        <p> ciência da saúde que tem por objeto de estudo o movimento humano e seu funcionamento adequado, avaliando, prevenindo e tratando as alterações genéticas, traumas e/ou doenças adquiridas nos órgãos e sistemas do corpo humano que afetem os distúrbios cinéticos funcionais (cinesia humana). Fundamenta suas ações em mecanismos terapêuticos próprios, sistematizados pelos estudos da biologia, das ciências morfológicas. Fisiológicas, patológicas, bioquímicas, biofísicas, biomecânicas, cinesioterápicas, além das disciplinas sociais e comportamentais.</p>
                    </div>

                    <div class="img-filterDiv">
                        <img src="[template]/pw-images/medicos.png" alt="" title="">
                    </div>

                </div>

            </div>


        </div>

        <a href="[url]/profissionais">
            <h3>Conheça nossos profissionais</h3>
        </a>



    </div>

</div>