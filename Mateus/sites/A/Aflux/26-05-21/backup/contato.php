<?php /* Template name: Contato */ ?>

<?php get_header(); ?>

<div class="conteudo-pages">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="titulo-total">
        <h1><?php echo get_the_title(); ?></h1>
    </div>

    <div class="formulario">

        <form action="pw-form.php" method="post">

            <div class="item">
                <input name="campo[Nome]" type="text" placeholder="Nome:" required/>
                <input name="campo[Sobrenome]" type="text" placeholder="Sobrenome:" required/>
                <input name="campo[E-mail]" type="text" placeholder="E-mail:" required/>
                <input name="campo[Cidade]" type="text" placeholder="Cidade:" required/>
                <input name="campo[Estado]" type="text" placeholder="Estado:" required/>
                <input name="campo[Telefone]" type="text" placeholder="Telefone:" required/>
                <input name="campo[Celular]" type="text" placeholder="Celular:" required/><br>
                <div class="radio">
                    <div class="fisica"><input type="radio" name="Pessoa">Pessoa Física </div>
                    <div class="juridica"><input type="radio" name="Pessoa">Pessoa Jurídica </div>
                    <div class="juridica-result">
                        <div class="juridica"><input type="radio" name="Inscricao">Inscrição Estadual </div>
                        <div class="juridica"><input type="radio" name="Inscricao">Isento</div>
                    </div>
                </div>
            </div> <!-- item -->
            <div class="item">
                <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
            </div> <!-- item -->
            <input class="submit" type="submit" value="Enviar" />

        </form>
        <div class="contatos">

            <div class="item">E-mail:</div>
            <div class="itens"><i class="fas fa-envelope"></i> comercial@afluxbrasil.com.br</div><br>

            <div class="item">Telefone:</div>
            <div class="itens"><i class="fa fa-phone" aria-hidden="true"></i> +55 11 2376-9961 / 2376-9965</div>

            <div class="item">Celular:</div>
            <div class="itens"><i class="fa fa-phone" aria-hidden="true"></i> +55 11 98987-9151 / 98983-5189</div>

            <div class="item">Endereço:</div>
            <div class="itens"><i class="fas fa-map-marker-alt"></i> Rua Lisboa, 41 - Osvaldo Cruz
                CEP: 09570-510 - São Caetano do Sul - SP</div>

        </div>


    </div> <!-- Formulario -->

    <?php endwhile; ?>
    <?php endif; ?>

</div> <!-- conteudo-pages -->
<div class="menu-produto-total">
    <div class="menu-produtos">
        <?php $posts = get_posts(array('post_type' => 'produtos_post', 'numberposts' => -1, 'order' => ASC ));?>
        <?php foreach($posts as $post){ ?>
        <?php setup_postdata($post); ?>
        <a href="<?php echo get_permalink(); ?>">
            <h2><?php echo get_field('titulo_menu');?></h2>
        </a>
        <?php } ?>
    </div>
</div>
<script>


</script>
<?php get_footer(); ?>
