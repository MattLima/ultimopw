<?php
	$dadosPagina["titulo"]   = "Resinas em São Paulo";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Polímeros Impermeabilizantes Compatíveis com Concentrados de Pigmentos Dispersos em Água, Resinas Acrílicas e Polímeros Opacos.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Resinas em São Paulo\" />";
  $dadosPagina["css"] = "";
?>

<div class="conteudo-pages">

	<div class="texto-pages">

		<h2>Resinas</h2>

      <h3><i class="fas fa-arrow-circle-right"></i> Polímero para impermeabilização - GP IMP 57</h3>
      <div class="produtos-total">
        <div class="produto-desc">
          <p>GP IMP 57 é um polímero com tecnologia inovadora especialmente desenvolvida para impermeabilização e promover resistência à água de fácil aplicação e ecologicamente correto. Indicada para uso em argamassas, adesivos flexíveis e manta líquida promovendo uma excelente impermeabilização. Apresenta boa elasticidade e é compatível com a maioria dos espessantes acrílicos, com ótima compatibilidade com concentrados de pigmentos dispersos em água.</p>
          <br>
          <b>COMPOSIÇÃO BÁSICA: Copolímero Acrílico em Emulsão.</b>
        </div>
        <div class="produtos-img">
          <img src="[template]/pw-images/produtos/polimero-gp-imp-57.png" alt="Polimero GM IMP 57" title="Polimero GM IMP 57" />
        </div>
      </div>

			<h3><i class="fas fa-arrow-circle-right"></i> Polímero Opaco - GP WHITE 19</h3>
      <div class="produtos-total">
        <div class="produto-desc">
          <p>GP WHITE 19 é um polímero opaco em solução aquosa de copolímero estireno acrílica com tecnologia inovadora utilizada como extensor de Dióxido de Titânio desenvolvido para utilização em diversas formulações para promover reduções de custo mantendo o desempenho das tintas. Funciona como um espaçador de pigmentos que aumenta a refração da luz evitando assim a perda de eficiência do TiO2 onde cargas finas convencionais aumentam muito a absorção e demanda de resina e consequentemente as propriedades e resistência da película das tintas.</p>

          <p>Vantagens:</p>
          <ul>
            <li><i class="fas fa-check"></i> Redução da porosidade da película</li>
            <li><i class="fas fa-check"></i> Aumento da lavabilidade</li>
            <li><i class="fas fa-check"></i> Melhoria da retenção de cor</li>
            <li><i class="fas fa-check"></i> Diminuição da pega e sujeira</li>
            <li><i class="fas fa-check"></i> Redução de custo com o mesmo desempenho</li>
            <li><i class="fas fa-check"></i> Eficiente em todos os níveis de PVC</li>
          </ul>
          <b>COMPOSIÇÃO BÁSICA: Copolímero estireno acrílico em emulsão.</b>
          <br>
          <br>
        </div>
        <div class="produtos-img">
          <img src="[template]/pw-images/produtos/polimero-gp-pack-19.jpg" alt="" title="" />
        </div>
      </div>

			<h3><i class="fas fa-arrow-circle-right"></i> Resina Acrílica - GP 727</h3>
        <div class="produtos-total">
          <div class="produto-desc">
            <p>GP 727  é um copolímero estireno acrílico com tecnologia inovadora especialmente desenvolvido para formulações de tintas com baixo odor. Utilizada como ligante na fabricação de tintas para residências, hospitais, escolas e complementos base água. Proporciona excelente resistência a lavabilidade e formação de um filme transparente, sendo compatível com diversos plastificantes, coalescentes e pigmentos do mercado.</p>
            <br>
            <b>Denominação Química: Copolímero acrílico em emulsão.</b>
          </div>
          <div class="produtos-img">
            <img src="[template]/pw-images/produtos/resina-acrilica.png" alt="" title="" />
          </div>
        </div>

	</div><!-- Texto Pages -->

</div> <!-- Conteudo Pages -->
