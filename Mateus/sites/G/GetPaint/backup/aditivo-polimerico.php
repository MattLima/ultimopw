<?php
	$dadosPagina["titulo"]   = "Aditivo Polimérico";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Aditivo Polimérico Especialmente Desenvolvido para Redução de Poeiras em Mineradora e Obras em Vias Não Asfaltadas.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Aditivo Polimérico\" />";
  $dadosPagina["css"] = "";
?>

<div class="conteudo-pages">

	<div class="texto-pages">

    <h2>Aditivos</h2>

  		<h3><i class="fas fa-arrow-circle-right"></i> Aditivo Polimerico - GP ANTI-DUST</h3>
  			<div class="produtos-total">
  				<div class="produto-desc">
  					<p>GP Anti Dust é um aditivo polimerico especialmente desenvolvido para redução de poeiras em mineradora e obras em vias não asfaltadas.</p>

  					<p>GP Anti Dust forma um película uniforme após aplicação reduzindo 95% da poeira com trafego de caminhões ou carros.</p>
  					<br>
  					<b>COMPOSIÇÃO QUÍMICA: Composto polimérico acrílico e aditivos especiais reológicos.</b>
						<br>
						<b>Embalagens 50 kgs, 180 kgs e 1000 kgs.</b>
  				</div>
  				<div class="produtos-img">
  					<img src="[template]/pw-images/produtos/gp-anti-dust.png" alt="GP Anti Dust" title="GP Anti Dust" />
  				</div>
  			</div>

	</div><!-- Texto Pages -->

</div> <!-- Conteudo Pages -->
