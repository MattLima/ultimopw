<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
   
   
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
     <style><?php echo file_get_contents('template/pw-css/style.css');?></style> 
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">
    <link rel="stylesheet" href="pw-css/swiper.min.css">   <!--swiper da pagina empresa -->
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }

    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157818717-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-157818717-15');
</script>

</head>

<body>



    <div class="linha-total">

        <div class="linha">
            <div class="item"><i class="fas fa-phone"></i> (19) 3886-3156 / 3886-1821</div>
            <!-- Item -->
            <div class="item"><i class="fas fa-envelope"></i> contato@serafinsimportadora.com.br</div>
            <!-- Item -->
            <div class="item"><i class="fas fa-map-marker-alt"></i> Rua Graciosa Trevisan Saltori, 44 - Nova Vinhedo - Vinhedo/SP - CEP: 13284-140</div>
            <!-- Item -->
        </div>
        <!-- linha -->

    </div>
    <!-- Linha Total -->

    <div class="topo-total">

        <div class="topo">

            <div class="logo"><a target="blank" href="[url]/" title=""><img src="[template]/pw-images/logo-serafins-importadora.png" alt="Serafins Importadora" title="Serafins Importadora" /></a></div>
            <!-- Logo -->
<div class="menu-resp"></div>
            <div class="menu">

                <ul>
                    <li><a href="[url]/" title="HOME" data-icon="empresa">HOME</a></li> <!-- [url]/ -->
                    <li><a href="[url]/empresa-de-importadora-de-vinhos-variados-serafins" title="EMPRESA" data-icon="empresa">EMPRESA</a></li>
                    <li><a href="[url]/produtos-da-melhor-importadora-de-vinhos-e-ferramentas-de-construcao" title="PRODUTOS" data-icon="produtos">PRODUTOS</a></li>
                    <li><a href="[url]/seja-um-representante-da-importadora-de-vinhos-e-ferramentas-de-construcao" title="REPRESENTANTES" data-icon="representantes">REPRESENTANTES</a></li>
                    <li><a href="[url]/entre-contato-com-a-melhor-importadora-de-vinhos" title="CONTATO" data-icon="CONTATO">CONTATO</a></li>
                </ul>

            </div>
            <!-- Menu -->

        </div>
        <!-- Topo -->

    </div>
    <!-- Topo Total -->

    <div class="global">

        [conteudo]

    </div>
    <!-- global -->
    

    <div class="rodape-total">

        <div class="rodape">

            <div class="item">
            <i class="fa fa-phone" aria-hidden="true"></i> (19) 3886-3156 / 3886-1821 <br> 
            <a href="https://api.whatsapp.com/send?phone=5511993645756" target="_blank">  <i class="fab fa-whatsapp"> <p> (11) 99364-5756 </p></i>  </a>
            </div>
            <!-- Item -->
            <div class="item"><i class="fas fa-envelope"></i> contato@serafinsimportadora.com.br</div>
            <!-- Item -->
            <div class="item"><i class="fas fa-map-marker-alt"></i> Rua Graciosa Trevisan Saltori, 44 - Nova Vinhedo - Vinhedo/SP - CEP: 13284-140</div>
            <!-- Item -->

        </div>
        <!-- Rodape -->
        <div class="logo-pw">
            <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
            <div>
                <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                <p>
                    <a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank"><span>Criação de Sites /</span></a>
                    <a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites</span></a>
                    <a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site</span></a>
                    <a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site</span></a>
                    <a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites</span></a>
                    <a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas</span></a>
                    <a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
                </p>
            </div>
        </div>

        <div class="faixa-rodape" style="display: none;">
            <?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php ');?>
        </div>



    </div>
    <!-- Rodape Total -->

    <script>
        <?php echo file_get_contents('pw-js/jquery.js');?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/scrollReveal.js');?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/javascript.js');?>

    </script> 
    <script>
     <?php echo file_get_contents('pw-js/swiper.min.js');?>  /* swiper pag empresa */
     

    </script>
    <script src="[template]/pw-slider-engine/wowslider.js"></script>
    <script src="[template]/pw-slider-engine/script.js"></script>
    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.box-02 .item', {
                duration: 2000,
                origin: 'right',
                distance: '100px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
            sr.reveal('.box-03 input, .box-03 textarea', {
                duration: 2000,
                origin: 'bottom',
                distance: '50px',
                viewFactor: 0.6,
                mobile: false
            }, 100);

            sr.reveal('.empresa-texto1', {
                duration: 2000,
                origin: 'right',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);

            sr.reveal('.imgempresa', {
                duration: 2000,
                origin: 'left',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);

            sr.reveal('.right', {
                duration: 2000,
                origin: 'right',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);

            sr.reveal('.left', {
                duration: 2000,
                origin: 'left',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);

            sr.reveal('.representantes3', {
                duration: 2000,
                origin: 'top',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);
            sr.reveal('.representantes4', {
                duration: 2000,
                origin: 'bottom',
                easing: 'ease-in-out',
                distance: '300px',
                viewFactor: 0.1,
                mobile: false
            }, 100);

            sr.reveal('.mapa', {
                duration: 2000,
                origin: 'bottom',
                distance: '0px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }

    </script>
</body>

</html>
