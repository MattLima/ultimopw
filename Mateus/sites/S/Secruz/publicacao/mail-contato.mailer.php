<?php	

	// Version: 0.01 //
	require_once("./class.phpmailer.php");
	require_once("./class.smtp.php");
	
	session_start();
	$ip = $_SERVER['REMOTE_ADDR'];
	
	if(isset($_SESSION['ip'])){
		if($_SESSION['ip'] == $ip) {
			$_SESSION['num']++;
			if($_SESSION['num'] > 2) {
				header("Location: http://$site/");
				exit;
			}
		}
	}
	else{
		$_SESSION['num'] = 1;	
		$_SESSION['ip'] = $ip;
	}
	
	if(!isset($_POST['campo'])) die();
	$campos = $_POST['campo'];
	
	if(strpos($campos['Mensagem'],'<a href') != false) die();
	
	$email_destinatario = "comercial@gruposecruz.com.br"; //Alterar para o e-mail do cliente
	$form_servidor		= "form@gruposecruz.com.br"; //Inserir o form pelo dominio do cliente
	$nome = "Formulario <$form_servidor>";
		 
	$site = $_SERVER['HTTP_HOST'];	
	date_default_timezone_set('America/Araguaina');
	$hora = date('d / M / Y - H:i');
	$email_assunto = "Novo contato do site: $site";
	
	if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
	elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows

	$email_conteudo = "<b>Data do envio:</b> $hora <br />\n";
	$email_conteudo .= "<b>IP:</b> $ip <br />\n<br />\n";
	
	foreach($campos as $key => $value){
		$email_conteudo .= "<b>$key: </b>" . $value . "<br />\n"; 
	}

	$header  = "From: " . $nome . $quebra_linha;
	$header	.= "Reply-To: ".$campos['E-mail'] . $quebra_linha;
	$header .= "Content-type: text/html; charset=utf-8".$quebra_linha;
	$header .= "Return-Path: " . $form_servidor . $quebra_linha; // Se "não for Postfix"
	$header	.= "Cc: $Cc".$quebra_linha;
			
	$arr1 = array("<b>","</b>","<br />", "\n");
	$arr2 = array("","","", chr(13).chr(10));
	
	$date = date("Y-m-d-H-i-s");
	
	$txt = str_replace($arr1, $arr2, $email_conteudo);
	
	if(!file_exists('formulario')) mkdir('formulario');
	$fp = fopen("formulario/".$date.".txt", "w");
	 
	$escreve = fwrite($fp, $txt);
	 
	fclose($fp); 

	$mail = new PHPMailer(true);
	$mail->isSMTP();
	$mail->Host = ""; // Aqui vai o servidor SMTP usado para enviar e-mails
	$mail->SMTPAuth = true;
	$mail->Port = 587;
	$mail->Username = ""; // Aqui vai o usuário para fazer o login no servidor SMTP
	$mail->Password = ""; // Aqui vai a senha do SMTP

	$mail->setFrom($form_servidor, $nome); // O setFrom define o remetente passando (email, nome)
	$mail->addAddress($email_destinatario, $email_destinatario); // O addAddress adiciona um destinatário passando (email, nome)
	$mail->addCC($Cc, $Cc); // O addCc adiciona uma cópia CC passando (email, nome)
	$mail->addReplyTo($campos['E-mail']); // Adiciona o Reply-To no header
		
	$mail->Subject = $email_assunto; // Define o assunto do e-mail
	$mail->Body = $email_conteudo; // Define o conteudo como HTML;
		
	if ($mail->send()) {
		header("Location: http://$site/confirma"); 
	} else {
		print_r('ERRO');
	}
		

?>
 