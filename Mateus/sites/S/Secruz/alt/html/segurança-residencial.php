<?php
	$dadosPagina["titulo"]   = "Instalação, manutenção e monitoramento";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Centrais de alarme 24h, CFTV, Sensores anti-fumaça, Equipamentos Segurança Eletrônica, Interfones Eletrônicos\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Instalação, manutenção e monitoramento\" />";
	$dadosPagina["metas"][2] = "";
	$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>SEGURANÇA ELETRÔNICA</h1>
    <div class="empresa-total">
        <div class="texto-empresa">

           
            <div class="item-text seguranca">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> Instalação, Manutenção e Monitoramento.</h2>
                    <p>
                    Instalação, manutenção e monitoramento de centrais de alarmes 24h, CFTV, sensores anti-fumaça, através de equipamentos de última geração e eficiência tecnológica.
                    </p>
                    <p>
                        Temos também toda a linha de cerca, portões e interfones eletrônicos para uma maior comodidade e segurança de sua residência, comércio ou indústria. Para comercialização destes produtos e serviços de segurança eletrônica temos as opções de venda e locação, facilitando assim a contratação de acordo com a sua necessidade.
                    </p>
                </div>
          
           
              <div class="item"><img src="[template]/pw-images/camera-de-seguranca.jpg" alt=""></div>

            </div>
        </div>


    </div>
 
</div>

