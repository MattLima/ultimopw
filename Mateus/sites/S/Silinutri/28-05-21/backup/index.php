<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 7";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 7\" />";
    $dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
    $dadosPagina["css"] = "<style></style>";
?>

<div class="fundo-slider">
<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0" /></li>
            <li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1" /></li>
        </ul>
    </div>

    <div class="ws_shadow"></div>
</div>
<!-- End WOWSlider.com BODY section -->
</div> <!-- Fundo Slider -->



<div class="box-01-total">

<div class="box-01">
    
    <a href="[url]/nutricao-clinica" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">NUTRIÇÃO<br> CLÍNICA
</div> <!-- Titulo -->
        <div class="detalhe">Visa o tratamento de mulheres adultas
</div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>
    <a href="[url]/gestantes-e-nutrizes" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">GESTANTES E NUTRIZES</div> <!-- Titulo -->
        <div class="detalhe">Acompanhamento gestacional e pós -parto</div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>
    <a href="[url]/nutricao-e-estetica" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">NUTRIÇÃO E ESTÉTICA </div> <!-- Titulo -->
        <div class="detalhe">Controle de peso corporal de modo a atuar em sinergia com tratamentos estéticos </div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>
    <a href="[url]/personal-diet" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">PERSONAL<br> DIET</div> <!-- Titulo -->
        <div class="detalhe">Atendimento à domicílio </div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>
    <a href="[url]/nutricao-na-menopausa" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">NUTRIÇÃO NA MENOPAUSA</div> <!-- Titulo -->
        <div class="detalhe">Atenção principal às alterações hormonais desse período </div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>
    <a href="[url]/cirurgia-bariatrica" title="">
    <div class="item">
        <div class="img"><i class="fas fa-apple-alt"></i></div> <!-- Img -->
        <div class="titulo">CIRURGIA BARIÁTRICA </div> <!-- Titulo -->
        <div class="detalhe">Programa pré e pós Operatório 

</div> <!-- Detalhe -->
        <div class="entrar">SAIBA MAIS</div> <!-- Entrar -->
    </div> <!-- Item -->
    </a>

</div> <!-- Box 01 -->

</div> <!-- Box 01 Total -->



<div class="box-02-total">

	<div class="box-02">
    
    	<div class="titulo">A EMPRESA</div> <!-- Titulo -->
        <div class="conteudo">
        	Atendimento com nutricionista especializada na área, para mulheres nas diversas etapas da vida,através de consultas, atendimentos online e à domicílio. 
        </div> <!-- Conteudo -->
        <a href="" title=""><div class="entrar">Entrar</div></a> <!-- Entrar -->
    
    </div> <!-- Box 02 -->

</div> <!-- Box 02 Total -->




<div class="box-03-total">

	<div class="box-03">
   
   <div class="titulo"> SERVIÇOS</div>
    
    	
        
        <div class="box">
        
        	<a href="" title="">
        	<div class="item">
            	<div class="img"><img src="[template]/pw-images/index-01.jpg" alt="" title="" /></div> <!-- Img -->
                <div class="titulo">AVALIAÇÃO<br> NUTRICIONAL </div> <!-- Titulo -->
                <div class="data">Histórico clínico e alimentar, avaliação fisica, diagnóstico através de exames bioquímicos,análise funcional.</div> <!-- Data -->
            </div> <!-- Item -->
            </a>
        	<a href="" title="">
        	<div class="item">
            	<div class="img"><img src="[template]/pw-images/index-02.jpg" alt="" title="" /></div> <!-- Img -->
                <div class="titulo">PLANEJAMENTO DE COMPRAS</div> <!-- Titulo -->
                <div class="data">Indicação de produtos quanto à sua qualidade e quantidade,interpretação dos rótulos de embalagens de produtos da despensa ou do supermercados, hortifrutigranjeiros. </div> <!-- Data -->
            </div> <!-- Item -->
            </a>
        	<a href="" title="">
        	<div class="item">
            	<div class="img"><img src="[template]/pw-images/index-03.jpg" alt="" title="" /></div> <!-- Img -->
                <div class="titulo">CONFEÇÃO DE RECEITAS E CARDÁPIOS </div> <!-- Titulo -->
                <div class="data">Orientação para confeção de receitas saudáveis e práticas a domicílio , para o próprio paciente, cozinheiros ou cuidadores.Planejamento de cardápios levando em consideração os hábitos alimentares da família, opções de refeições prontas frescas ou congeladas .</div> <!-- Data -->
            </div> <!-- Item -->
            </a>
        	
            
        </div> <!-- Box -->
    
    </div> <!-- Box 03 -->

</div> <!-- Box 03 Total -->



<div class="box-04-total">

	<div class="box-04">
    
    	<div class="titulo">CONTATE-NOS</div> <!-- Titulo -->
        <div class="contato">
        
        	<form action="pw-form.php" method="post">
            
            	<input name="campo[Nome]" type="text" placeholder="Nome:" />
            	<input name="campo[E-mail]" type="text" placeholder="E-mail:" />
            	<input name="campo[Telefone]" type="text" placeholder="Telefone:" />
            	<input name="campo[Empresa]" type="text" placeholder="Empresa:" />
                <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
            	<input value="Enviar" type="submit" class="submit" />
            
            </form>
                    
        </div> <!-- Contato -->
    
    </div> <!-- Box 04 -->

</div> <!-- Box 04 Total -->



<div class="mapa">
            	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7343.835747945324!2d-46.98719114285276!3d-23.026787452032067!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92328269928cdb62!2sAg%C3%AAncia+Projeto+Web+-+Empresa+de+Cria%C3%A7%C3%A3o+de+Sites!5e0!3m2!1spt-BR!2sus!4v1478803498533" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>

</div> <!-- Mapa -->



