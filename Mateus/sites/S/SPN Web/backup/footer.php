<?php wp_footer(); ?>

<div class="rodape-total">

<div class="btnWpp">
    <a target="_blank" href="https://api.whatsapp.com/send?phone=5571991663671&text=Ol%C3%A1%2C%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es!%20"><i class="fab fa-whatsapp"></i></a>
</div>

<div class="rodape">

	<div class="conteudo-rodape">

		<div class="texto-rodape">
			<i class="fas fa-phone-volume"></i><br>
			<span><?php echo get_field('telefone', 13);?> <br><span class="icon-rodape"><i class="fab fa-whatsapp"></i></span><?php echo get_field('whatsapp', 13);?></span>
		</div>
		<!-- Texto Rodape -->
		<div class="texto-rodape">
			<i class="far fa-envelope"></i><br>
			<span><?php echo get_field('email', 13);?></span>
		</div>
		<!-- Texto Rodape -->
		<div class="texto-rodape endereco">
		   <i class="fas fa-map-marker-alt"></i><br>
			<p><?php echo get_field('endereco', 13);?></p>
		</div>
		<!-- Texto Rodape -->
		<div class="texto-rodape contatos">
			<a href=""><div class="item chat fim"><i class="far fa-comments"></i> Atendimento Online</div></a>
		</div>
		<!-- Texto Rodape -->

        <div class="btnTop">
            <a href="#topoSite"><i class="fas fa-arrow-circle-up"></i><br>Voltar ao Topo</a>
        </div>

	</div>
	<!-- conteudo rodape -->


</div>
<!-- Rodape -->
	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/swiper.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-slider-engine/wowslider.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-slider-engine/script.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/javascript.js"></script>
	<script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.topo', {
                duration: 2000,
                origin: 'top',
                distance: '50px',
                viewFactor: 0.6
            }, 100);
            sr.reveal('.box-01 .box', {
                duration: 2000,
                origin: 'top',
                distance: '50px',
                viewFactor: 0.6
            }, 300);
            sr.reveal('.box-02 .texto', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                viewFactor: 0.2
            }, 300);
            sr.reveal('.box-03 .conteudo-total', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                viewFactor: 0.2
            }, 300);
            sr.reveal('.box-03 .img', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                viewFactor: 0.2
            }, 300);
            sr.reveal('.box-04 .conteudo .item', {
                duration: 2000,
                origin: 'bottom',
                distance: '50px',
                viewFactor: 0.2
            }, 500);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }
    </script>
</body>
</html>