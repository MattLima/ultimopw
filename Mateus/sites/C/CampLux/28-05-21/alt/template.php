<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148407387-12"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-148407387-12');

	</script>
	<!-- Google ADS FIM -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

	<title>[pagina]</title>

	<link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	[css]

	<style>
		<?php echo file_get_contents('template/pw-css/style.css');
		?>
	</style>
	<style>
		<?php echo file_get_contents('pw-font-awesome/css/all.css');
		?>

	</style>
	<style>
		<?php echo file_get_contents('pw-galeria-fancybox/jquery.fancybox.min.css');
		?>

	</style>
	<style>
		<?php echo file_get_contents('pw-css/swiper.min.css');
		?>

	</style>

	<!-- Global site tag (gtag.js) - Google Ads: 1002239545 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1002239545"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'AW-1002239545');

	</script>
	
	<!-- Event snippet for Contato watts conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> <script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-1002239545/98NQCKCet9gBELns890D', 'event_callback': callback }); return false; } </script> 
	
	

</head>

<body>

	<div class="topo-total">

		<div class="topo">

			<div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo-camp-lux.png" alt="" title="" /></a></div>
			<!-- Logo -->

			<div class="dados">
			<a href="https://api.whatsapp.com/send?phone=5519981250574" onclick="return gtag_report_conversion" title="Assistência Técnica por Whatsapp" target="_blank"><div class="wppp"><img src="[template]/pw-images/wpp.png" alt=""><p> 
				 (19) 98125-0574</a></p></div>
				<span>(19) - 98909-6920  </span>
				<div class="tel-escondido"><p>(19) - 98909-6920</p></div>
				Av. Andrade Neves , 1180 - Campinas SP - CEP: 13013-161
			</div> <!-- dados -->

		</div>
		<!-- Topo -->

	</div>
	<!-- Topo Total -->

	<div class="menu-total">

		<div class="menu-resp"></div>
		<!-- Menu Resp -->

		<div class="menu">

			<ul>
				<li><a href="[url]" title="">Home</a></li>

				<li><a href="[url]/empresa-assistencia-tecnica-eletrodomesticos-secadora" title="Empresa de Assistência Técnica em Campinas">Empresa</a></li>

				<li><a href="[url]/lavadoras-conserto-maquina-de-lavar-filtro" title="Conserto de Máquina de Lavar">Lavadoras</a></li>

				<li><a href="[url]/purificador-filtro-eletrodomesticos-assistencia-tecnica" title="">Purificador de Água</a></li>

				<li><a href="[url]/lava-seca-geladeiras-conserto-secadora" title="">Lava e Seca</a></li>

				<li><a href="[url]/microondas-eletrodomesticos-conserto-assistencia-tecnica" title="">Microondas</a></li>

				<li><a href="[url]/refrigeradores-geladeira-purificador-filtro-conserto" title="">Refrigerador</a></li>

				<li><a href="[url]/assistencia-tecnica-venda-filtros-campinas" title="">Venda de Peças e Acessórios</a></li>
			</ul>

		</div>
		<!-- Menu -->

	</div>
	<!-- menu Total -->

	<div class="global">

		[conteudo]

	</div>
	<!-- global -->

	<div class="rodape-total">

		<div class="rodape">

			<div class="logo-rodape"><a href="[url]/" title=""><img src="[template]/pw-images/logo-camp-lux-rodape.png" alt="" title="" /></a></div>
			<!-- logo Rodape -->

			<div class="texto-rodape">
				<div class="item">camp-lux@hotmail.com</div>
				<div class="item"><!-- (19) 3251-9241 / 3236-0937 --></div>
			</div>
			<!-- Texto Rodape -->

			<div class="redes-rodape">
				<div class="item facebook">Facebook</div>
				<div class="item instagram">Instagram</div>
				<div class="item twitter">Twitter</div>
			</div>
			<!-- Texto Rodape -->



		</div>
		<!-- Rodape -->

		<div class="logo-pw">
			<a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
			<div>
				<p><a title="" href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
				<p>
					<a title="" href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
					<a title="" href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites/</span></a>
					<a title="" href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site/</span></a>
					<a title="" href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site/</span></a>
					<a title="" href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites/</span></a>
					<a title="" href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas/</span></a>
					<a title="" href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
				</p>
			</div>
		</div>

		<div class="faixa-rodape" style="display: none;">
			<?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php');?>
		</div>

	</div>
	<!-- Rodape Total -->

	<script>
		<?php echo file_get_contents('pw-js/jquery.js');?>

	</script>
	<script>
		<?php echo file_get_contents('pw-js/swiper.min.js');?>

	</script>
	<script>
		<?php echo file_get_contents('pw-js/javascript.js');?>

	</script>
	<script>
		<?php echo file_get_contents('pw-js/scrollReveal.js');?>

	</script>
	<script src="[template]/pw-slider-engine/wowslider.js"></script>
	<script src="[template]/pw-slider-engine/script.js"></script>
	<script src="pw-galeria-fancybox/jquery.fancybox.min.js"></script>
	<script>
		(function($) {
			'use strict';
			window.sr = ScrollReveal();
			sr.reveal('.menu', {
				duration: 2000,
				origin: 'bottom',
				distance: '100px',
				viewFactor: 0.6,
				mobile: false
			}, 100);
			sr.reveal('.slider', {
				duration: 2000,
				origin: 'bottom',
				distance: '100px',
				viewFactor: 0.6,
				mobile: false
			}, 100);
			sr.reveal('.box-01 .item', {
				duration: 2000,
				origin: 'left',
				distance: '100px',
				viewFactor: 0.6,
				mobile: false
			}, 500);
			sr.reveal('.box-02 img', {
				duration: 1000,
				origin: 'top',
				distance: '100px',
				viewFactor: 0.6,
				mobile: false
			}, 200);
			sr.reveal('.box-03', {
				duration: 2000,
				origin: 'bottom',
				distance: '100px',
				viewFactor: 0.6,
				mobile: false
			}, 100);
			sr.reveal('.box-04', {
				duration: 2000,
				origin: 'left',
				distance: '200px',
				viewFactor: 0.6,
				mobile: false
			}, 100);
		})();

		$(document).ready(function() {
			var hash = window.location.hash;
			if (hash == '#pw_site') {
				$('.faixa-rodape').css('display', 'block');
			}
		});

		function isMobile() {
			var userAgent = navigator.userAgent.toLowerCase();
			return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
		}

		if (isMobile()) {
			jQuery(document).ready(function() {
				var page = $(location).attr('href');
				if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
					jQuery('html, body').animate({
						scrollTop: $("h1").offset().top
					}, 500);

				}
			});
		}

	</script>
</body>

</html>
