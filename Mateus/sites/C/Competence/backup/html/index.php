<?php
$dadosPagina["titulo"]   = "Escola de Formação Cristã - Competence";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Escola de Formação Cristã. Escola, Escola de formação, Educação Infantil, Colégio Infantil, Escola Infantil, Escolas Particulares.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Escola de Formação Cristã - Competence\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
?>

<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0" /></li>
            <li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1" /></li>
        </ul>
    </div>

    <div class="ws_shadow"></div>
</div>
<!-- End WOWSlider.com BODY section -->

<div class="box-01-total">

    <a class="entrar" href="[url]/escola-particular-crista" title="Escola Particular Cristã " data-icon="empresa">
        <div class="box-01">

            <div class="conteudo">
                <div class="titulo">BEM-VINDO AO<br />NOSSO SITE</div>
                <div class="linha">
                    <div class="barra"></div>
                </div>
                <div class="texto">
                    Seja bem-vindo ao nosso espaço virtual e à nossa escola. Estamos muito felizes com seu interesse em conhecer mais da nossa história e de como chegamos até aqui. Se você está aqui é porque certamente já ouviu falar da CEI e dos nossos excelentes resultados, tanto em seletivos quanto na vida. Nossos alunos são ensinados com o que há de melhor.
                </div>
                <div class="entrar">Saiba mais</div>
            </div>
            <div class="img">
                <img src="[template]/pw-images/higienizacao.jpg" alt="Escola Particular Cristã " title="Escola Particular Cristã " />
                <div class="info"><i class="far fa-thumbs-up"></i><br />"Atender Bem Para Atender Sempre"</div>
            </div>

        </div> 
    </a>

</div> 

<div class="box-02-total">

    <div class="box-02">

        <a class="conteudo" href="[url]/escola-particular-crista" title="Escola Particular Cristã " data-icon="escola-formacao">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/higienizacao.jpg" alt="Escola Particular Cristã " title="Escola Particular Cristã " /></div>
                <div class="titulo">NOSSA HISTÓRIA</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>
        <a class="conteudo" href="[url]/escola-crista" title="Escola Cristã " data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/escola-crista.jpg" alt="Escola Cristã " title="Escola Cristã " /></div>
                <div class="titulo">MODALIDADES DE ENSINO</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>
        <a class="conteudo" href="[url]/escola-infantil-particular" title="Escola Infantil Particular " data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/escola-infantil-particular.jpg" alt="Escola Infantil Particular " title="Escola Infantil Particular " /></div>
                <div class="titulo">LOCALIZAÇÃO</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin.
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>

    </div>

</div>

<div class="box-03-total">

    <div class="box-03">

        <div class="linha">
            <div class="barra"></div>
        </div>
        <div class="titulo">ENTRE EM <br />CONTATO</div>

        <form action="mail-contato.php" method="post">
            <div class="item">
                <input name="campo[Nome]" type="text" placeholder="Nome:" />
                <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
            </div>
            <div class="item">
                <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
            </div>
            <input class="submit" type="submit" value="Enviar" />
        </form>

    </div>

</div>

<div class="mapa">

    <div class="mapa-competence">
       <a target="_blank" href="https://www.google.com/maps/place/Centro+Educacional+Competence/@-2.556416,-44.244505,16z/data=!4m5!3m4!1s0x0:0xa0af13cb08db2f85!8m2!3d-2.5564156!4d-44.2445054?hl=pt-BR">  <img src="[template]/pw-images/mapa-Competence.jpg" alt="">  </a> 
    </div>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3985.847605235831!2d-44.24669944940193!3d-2.556415598128152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7f6901c210c6499%3A0xa0af13cb08db2f85!2sCentro%20Educacional%20Competence!5e0!3m2!1spt-BR!2sbr!4v1617652927940!5m2!1spt-BR!2sbr" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>