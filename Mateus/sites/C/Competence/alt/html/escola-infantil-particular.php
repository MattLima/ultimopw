<?php
$dadosPagina["titulo"]   = "Escola Infantil Particular - Competence";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Escola Infantil Particular. Escola de Formação Cristã. Escola, Colégio particular, Escola de formação, Educação Infantil, Colégio Infantil, Escola Infantil, Escola Particular.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Escola Infantil Particular - Competence\" />";
?>

<div class="conteudo-pages">
    <div class="texto-pages">

        <h1>FALE CONOSCO</h1>

        <div class="formulario contato">

            <form action="mail-contato.php" method="post">

                <div class="item">
                    <input name="campo[Nome]" type="text" placeholder="Nome:" />
                    <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                    <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
                </div>
                <div class="item">
                    <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
                </div>
                <input class="submit" type="submit" value="Enviar" />

            </form>

            <div class="maps">

                <div class="mapa-competence">
                    <a href="https://g.page/escola-infantil-fundamental?share">  <img src="[template]/pw-images/mapa-Competence.jpg" alt="">  </a> 
                </div>

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3985.847605235831!2d-44.24669944940193!3d-2.556415598128152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7f6901c210c6499%3A0xa0af13cb08db2f85!2sCentro%20Educacional%20Competence!5e0!3m2!1spt-BR!2sbr!4v1617652927940!5m2!1spt-BR!2sbr"  height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

        </div>

    </div>
</div>