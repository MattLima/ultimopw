<?php
$dadosPagina["titulo"]   = "Colégio Infantil Particular - Competence ";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Colégio Infantil Particular. Escola de Formação Cristã. Escola, Escola de formação, Colégio Infantil, Escola Infantil, Ensino Integral, Escolas Particulares. \" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Colégio Infantil Particular - Competence\" />";
?>

<div class="conteudo-pages">
    <h1>BIBLIOTECA DE FOTOS</h1>
</div>

<div class="page">
    <div class="row single">

        <div class="gallery">


            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil1.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil1.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental1.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental1.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil1.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil1.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escolinha1.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escolinha1.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental2.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental2.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil2.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil2.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental2.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental2.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil2.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil2.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escolinha2.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escolinha2.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental3.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental3.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil3.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil3.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental3.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental3.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil3.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil3.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escolinha3.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escolinha3.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental4.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental4.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil4.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil4.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental4.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental4.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil4.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil4.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escolinha4.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escolinha4.jpeg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental5.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental5.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil5.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil5.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental5.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental5.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil5.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil5.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental6.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental6.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil6.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil6.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental6.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental6.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil6.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil6.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental7.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental7.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil7.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil7.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental7.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental7.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil7.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil7.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental8.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental8.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-infantil8.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-infantil8.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental8.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental8.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil8.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil8.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/escola-ensino-fundamental9.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-ensino-fundamental9.jpg" alt="" title="">
                </a>
            </div>
            
            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-fundamental9.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-fundamental9.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

            <div class="foto-gallery">
            <a href="[template]/pw-images/galeria/thumb/escola-infantil9.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/escola-infantil9.jpg" alt="Escola Fundamental" title="">
                </a>
            </div>

    </div>
</div>