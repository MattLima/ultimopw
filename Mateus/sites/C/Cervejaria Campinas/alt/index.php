<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PLKFN6S');

	</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92790401-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-92790401-2');

	</script>

	<style>
		/*POP UP*/
		.popup-total {
			position: fixed;
			display: flex;
			height: 100%;
			width: 100%;
			background: #0000009c;
			z-index: 999 !important;
			justify-content: center;
			align-items: center;

		}

		body {
			position: relative;
		}

		.popup-total .xis {
			display: flex;
			justify-content: flex-end;
		}

		.popup-total .xis * {
			font-size: 27px;
			color: #FAAF3F;
			cursor: pointer;
		}

		.popup-total .popup {
			max-height: 100%;
			max-width: 100%;
		}

		.popup-total .popup .imagem-popup img {
			max-height: 100%;
			max-width: 100%;
		}

		.popup-total .popup .imagem-popup {
			max-height: 100%;
			max-width: 100%;
		}

	</style>


	<script>
		function openModal() {
			document.getElementById("myModal").style.display = "block";
		}

		function closeModal() {
			document.getElementById("myModal").style.display = "none";
		}

	</script>

	<!-- The Modal -->



	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, user-scalable=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<title>Cervejaria Campinas</title>

	<meta property="og:url" content="" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Cervejaria Campinas" />
	<meta property="og:description" content="" />
	<meta property="og:image" content="images/logo-cervejaria-topo.png" />

	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/age-verification.css" type="text/css" />

	<!-- REACH CODE -->
	<script type="text/javascript">
		var rl_siteid = "3b452c9e-a451-457c-a761-8fb67d445c4f";

	</script>
	<!-- //REACH CODE -->

</head>




<?php

$menu='home';

    function SendSendGrid($from_name, $from_email, $to, $message, $subject, $reply_to)
   {

       $url = 'https://api.sendgrid.com/';
       $user = 'me@hagens.com.br';
       $pass = 'weareH@gens2017';

       $params = array(
           'api_user' => $user,
           'api_key' => $pass,
           'to' => $to,
           'subject' => $subject,
           'html' => $message,
           'from' => $from_email,
           'fromname' => $from_name,
           'replyto' => $reply_to
       );

       $request = $url . 'api/mail.send.json';

       // Generate curl request
       $session = curl_init($request);

       // Tell curl to use HTTP POST
       curl_setopt($session, CURLOPT_POST, true);

       // Tell curl that this is the body of the POST
       curl_setopt($session, CURLOPT_POSTFIELDS, $params);

       // Tell curl not to return headers, but do return the response
       curl_setopt($session, CURLOPT_HEADER, false);
       curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

       // obtain response
       $result = curl_exec($session);
       curl_close($session);

       //error_log('SENDGRID:' . var_export($result, true));

       return $result;
   }


//verifica se foi postado o form
if ($_POST['inserirfalk']=='true') {
	foreach (array_keys($_POST) as $p) {
		$$p = $_POST[$p];
	}
	//montando o email
	$subject = "Contato Site Cervejaria Campinas - ".date("d/m/Y");

	$message = '
			<html>
			<head>
			<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
			 <title>'.$subject.'</title>
			</head>
			<body>
			<br><font face="Verdana, Arial, Helvetica, sans-serif" size="3" color="#000066"><b>'.$subject.'</b><br><hr><br><br>
			<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
			<b>Nome:</b> '.$nome.'<br><br>
			<b>e-mail:</b> '.$email.'<br><br>
			<b>Mensagem:</b> '.str_replace(chr(10),'<br />',$itensDesejados).'<br><br>

			</font>
			</body>
			</html>';



			try {
               SendSendGrid('Site Cervejaria', 'me@hagens.com.br', 'marketing@cervejariacampinas.com.br', $message, $subject, $email);
            //    SendSendGrid('Site Cervejaria', 'me@hagens.com.br', 'ralph@hagens.com.br', $message, $subject, $email);
		        echo "<script>alert('Sua mensagem foi enviada corretamente! Em breve entraremos em contato.');</script>";
                //echo "<script>window.location='".$linksite."';</script>";

            } catch(Exception $e) {
                echo $e->getCode() . "\n";
                foreach($e->getErrors() as $er) {
                    echo $er;
                }
            }

}
?>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PLKFN6S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include "header.php"; ?>

	<section class="content slide">
		<div class="slide-list">
			<div class="item">
				<h1 class="title-h1">Uma cervejaria artesanal reconhecida no Brasil e no mundo</h1>
				<img src="images/cerveja-artesanal-cervejaria-campinas.png" alt="Uma Família Premiada">
			</div>
		</div>
	</section>
	<section class="content cervejeiros">
		<div class="middle">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="description">
						<h2 class="title-h2">Somos apaixonados por <strong><br>CERVEJA ARTESANAL</strong></h2>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="video">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/vf5kGVtbM8Q?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content style">
		<div class="middle">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h2 class="title-h2">Um estilo para<br>cada paladar</h2>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#01">
								<img src="images/garrafas/cerveja-artesanal-campinas-pilsen-cervejaria-campinas.jpg" alt="German PILSEN">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#01">German Pilsen</a></h3>
							<div class="text">
								<a href="nossos_estilos.php#01">CAMPINAS Pilsen - estilo tradicional alemão adequado ao paladar do brasileiro. Presença de lúpulos da República Tcheca e Malte tipo Pilsen da Alemanha.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/german-pilsen-355ml">German Pilsen na Loja Virtual</a>
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#02">
								<img src="images/garrafas/cerveja-artesanal-legionaria-weizen-cervejaria-campinas.jpg" alt="German WEIZEN">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#02">German Weizen</a></h3>
							<div class="text">
								<a href="nossos_estilos.php#02">CAMPINAS Legionária Weizen - Cerveja LEGIONÄRIA - (cerveja de trigo) - uma cerveja de trigo proveniente de uma receita tradicional alemã com sabor e aroma pouco mais tendencioso para banana, apesar do cravo ser também perceptível.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/legionaria-german-weizen-500ml">German Weizen na Loja Virtual</a>
					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#03">
								<img src="images/garrafas/cerveja-artesanal-andarilha-oatmeal-stout-cervejaria-campinas.jpg" alt="English Oatmeal STOUT">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#03">English Oatmeal Stout</a></h3>
							<div class="text"><a href="nossos_estilos.php#03">
									CAMPINAS Andarilha Outmeal Stout - Cerveja ANDARILHA receita legítima inglesa usando lúpulos ingleses de aroma amadeirado e cinco diferentes maltes, além de flocos de aveia para conferir cremosidade.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/english-oatmeal-stout-500ml">English Oatmeal Stout na Loja Virtual</a>
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#04">
								<img src="images/garrafas/cerveja-artesanal-hop-lager-cervejaria-campinas.jpg" alt="American HOP LAGER">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#04">American HOP Lager</a></h3>
							<div class="text"><a href="nossos_estilos.php#04">
									Campinas Hop Lager - a base do estilo é uma Czech Premium Pale Lager, o que seria a clássica Pilsen fabricada na República Tcheca.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/american-hop-lager-500ml">American Hop Lager na Loja Virtual</a>
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#05">
								<img src="images/garrafas/cerveja-artesanal-american-wheat-cervejaria-campinas.jpg" alt="American WHEAT">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#05">American Wheat</a></h3>
							<div class="text"><a href="nossos_estilos.php#05">
									CAMPINAS American Wheat - uma cerveja de trigo sim! Porém, como toda boa cerveja da escola americana tem o lúpulo como ator principal.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/forasteira-american-wheat-500ml">American Wheat na Loja Virtual</a>
					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#06">
								<img src="images/garrafas/cerveja-artesanal-amber-ale-cervejaria-campinas.jpg" alt="American AMBER ALE">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#06">American Amber Ale</a></h3>
							<div class="text"><a href="nossos_estilos.php#06">
									CAMPINAS Amber Ale - notas de caramelo muito presentes devido aos maltes ingleses com lúpulos americanos conferindo aroma cítrico.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/american-amber-ale-500ml">American Amber Ale na Loja Virtual</a>
					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#07">
								<img src="images/garrafas/cerveja-artesanal-forasteira-IPA-cervejaria-campinas.jpg" alt="American IPA">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#07">American IPA</a></h3>
							<div class="text"><a href="nossos_estilos.php#07">
									CAMPINAS Forasteira IPA - Cerveja FORASTEIRA IPA - Estilo American IPA que tem característico o aroma cítrico fazendo lembrar maracujá ao olfato devido ao lúpulo Amarillo e Galaxy entre outros tipos de lúpulo.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/forasteira-american-ipa">American Ipa na Loja Virtual</a>
					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#13">
								<img src="images/garrafas/cerveja-artesanal-IPAtodoDia-cervejaria-campinas.jpg" alt="American IPA">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#13">Session IPA</a></h3>
							<div class="text"><a href="nossos_estilos.php#13">
									CAMPINAS IPA Todo Dia - Derivada do estilo IPA e com elevadíssimo drinkability a Session IPA tem teor alcoólico reduzido porém sem perder o amargor e aroma cítrico que são característicos da escola americana.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/session-ipa-todo-dia-355ml">Session IPA na Loja Virtual</a>
					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#09">
								<img src="images/garrafas/cerveja-artesanal-eldorado-punch-IPA-cervejaria-campinas.jpg" alt="Eldorado PUNCH IPA">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#09">American IPA</a></h3>
							<div class="text"><a href="nossos_estilos.php#09">
									CAMPINAS Eldorado PUNCH IPA - Uma porrada nos seus sentidos! Cheia de aromas, sabores e amargor. Essa cerveja da Cervejaria Campinas apresenta uma cor bem dourada e é considerada forte.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/eldorado-punch-ipa-500ml">American Punch Ipa na Loja Virtual</a>
					</article>
				</div>
				<!--  -->

				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#10">
								<img src="images/garrafas/cerveja-artesanal-tank-IPA-cervejaria-campinas.jpg" alt="Imperial TANK IPA">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#10">Double IPA</a></h3>
							<div class="text"><a href="nossos_estilos.php#10">
									CAMPINAS Imperial TANK IPA - Uma IPA mais robusta, uma verdadeira batalha entre um amargor ALTO ou ABSURDAMENTE ALTO. Vai ser o seu paladar que vai decidir quem vence essa batalha.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/imperial-tank-ipa-500ml">Double IPA na Loja Virtual</a>
					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#12">
								<img src="images/garrafas/cerveja-artesanal-too-much-gose-cervejaria-campinas.jpg" alt="English ESB">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#12">Gose</a></h3>
							<div class="text"><a href="nossos_estilos.php#12">
									CAMPINAS TOO MUCH Gose - Uma receita bem peculiar com notas e sensações de condimentos e temperos. Experimente nossa cerveja com sal, tomate e pimenta.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/too-much-gose-355ml">Gose na Loja Virtual</a>
					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#11">
								<img src="images/garrafas/cerveja-artesanal-silvestre-sour-cervejaria-campinas.jpg" alt="Silvestre Sour">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#11">Catharina Sour</a></h3>
							<div class="text"><a href="nossos_estilos.php#11">
									CAMPINAS Silvestre Sour - pode ser uma cerveja sazonal até nas frutas, combinando amora e morango. Experimente o Diferente.</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/silvestre-sour-355ml">Silvestre Sour na Loja Virtual</a>
					</article>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#18">
								<img src="images/garrafas/ipa-zero-cerveja-artesanal-campinas.jpg" alt="IPA zero">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#18">IPA ZERO</a></h3>
							<div class="text"><a href="nossos_estilos.php#18">
							CAMPINAS IPA Zero - Os lúpulos americanos aplicados na maturação através do processo de Dry Hop, trazem pra nossa IPA Zero todas as características desse estilo: amargor e aroma intenso e ainda, sem álcool!</a>
							</div>
						</div>
						<a class="loja-virtual" href="https://loja.cervejariacampinas.com.br/campinas-ipa-zero-355ml-cerveja-sem-alcool">IPA ZERO na Loja Virtual</a>
					</article>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12">
					<article class="item">
						<div class="image">
							<a href="nossos_estilos.php#19">
								<img src="images/garrafas/azzapa-cerveja-artesanal-barril-campinas.jpg" alt="AZZAPA ">
							</a>
						</div>
						<div class="description">
							<h3 class="title-h3"><a href="nossos_estilos.php#19">AZZAPA - APA</a></h3>
							<div class="text"><a href="nossos_estilos.php#19">
							CAMPINAS Azzapa - no estilo American Pale Ale, mas uma legítima single hop: a AZZAPA! A combinação perfeita de 5 maltes, mais o lúpulo Azzaca, trouxe um resultado incrível, com um aroma mais intenso e bem mais cítrico.</a>
							</div>
						</div>
						
					</article>
				</div>
				


				

			</div>
		</div>
	</section>
	<section class="content tap-house">
		<div class="middle">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="description">
						<h2 class="title-h2">Saúde!</h2>
						<h3 class="title-h3">Venha conhecer a nossa Tap House em Campinas.</h3>
						<a href="tap_house.php" class="btn">Saiba mais</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="image">
						<img src="images/cervejaria-campinas-tap-house-cerveja-artesanal.png" alt="Venha conhecer a nossa Tap House em Campinas.">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content style-craft">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h2 class="title-h2">Conheça as nossas cervejas artesanais, produtos e souvenirs!</h2>
				<div class="slide-style-craft" id="js-accessories">
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/camisa.jpg" alt="Camisetas">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">Camisetas</a></h3>
					</div>
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/bone.jpg" alt="Bonés">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">Bonés</a></h3>
					</div>
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/growlers.jpg" alt="Growlers">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">Growlers</a></h3>
					</div>
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/kits.jpg" alt="kits Garrafas">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">kits Garrafas</a></h3>
					</div>
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/4pack.jpg" alt="4 Pack">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">4 Pack</a></h3>
					</div>
					<div class="item">
						<div class="image">
							<a data-toggle="modal" data-target="#modalAccessories">
								<img src="images/acessorios/tacas.jpg" alt="Taças e Canecas">
							</a>
						</div>
						<h3 class="title-h3"><a data-toggle="modal" data-target="#modalAccessories">Taças e Canecas</a></h3>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Modal -->
	<div class="modal fade" id="modalAccessories" tabindex="-1" role="dialog" aria-labelledby="modalAccessories" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h2 class="title-h2">Leve nosso estilo artesanal para casa!</h2>
						</div>
					</div>
					<div class="row">
						<!--  -->
						<div class="col-md-5 col-sm-5 col-xs-12 right">
							<div class="image"><img src="images/acessorios/camisa.jpg" alt="Camisetas"></div>
							<div class="image"><img src="images/acessorios/bone.jpg" alt="Bonés"></div>
							<div class="image"><img src="images/acessorios/growlers.jpg" alt="Growlers"></div>
							<div class="image"><img src="images/acessorios/kits.jpg" alt="Kits Garrafas"></div>
							<div class="image"><img src="images/acessorios/4pack.jpg" alt="4 Pack"></div>
							<div class="image"><img src="images/acessorios/tacas.jpg" alt="Taças e Canecas"></div>
						</div>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<div class="text">
								<p><strong>Todos nossos itens estão a venda em nossa Tap House:</strong></p>
								<p><strong>Horário de Funcionamento:</strong></p>
								<p>Rua Paula Bueno, 664 Taquaral, Campinas/SP</p>
								<p>(19) 3239-6742 - WhatsApp: (19) 99809-9787</p>
								<p>Terça a Sábado, das 10h às 23h<br>
									Domingo, das 10h às 19h</p>
								<div class="linha"></div><!-- Linha -->
								<p>Avenida Dermival Bernardes Siqueira, 2012 Swiss Park, Campinas/SP</p>
								<p>(19) 3203-7447 - Whatsapp: (19) 97168-4840</p>
								<p> Terça a Sexta, das 15h às 23h<br>
									Sábado, das 10h às 23h<br>
									Domingo, das 10h às 14h</p>

							</div>
							<form class="form-horizontal" id="form-validate" method="post" action="" enctype="multipart/form-data">
								<div class="text">
									<strong>Para consultar os valores, envie-nos uma mensagem:</strong>
								</div>
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="nome">Seu nome:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<input type="text" name="nome" id="nome" required>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="email">Seu e-mail:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<input type="email" name="email" id="email" required>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="itensDesejados">Itens desejados:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<textarea name="itensDesejados" id="itensDesejados" required class="form-control textarea-form" placeholder="Olá, gostaria dos valores dos itens:" rows="3">Olá, gostaria dos valores dos itens:</textarea>

										<!-- <input type="text" name="itensDesejados" id="itensDesejados" value="Olá, gostaria dos valores dos itens: "> -->
									</div>
									<div class="col-md-4 col-sm-4"></div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<!-- <a href="" class="btn-enviar">Consultar preços</a> -->
										<button type="submit" class="btn btn-lg btn-primary btn-enviar">Consultar preços</button>
										<input name="inserirfalk" type="hidden" id="inserirfalk" value="true" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="popup-total" id="popUp">

		<div class="popup">

			<div class="xis">
				<div class="xis-interno" onclick="document.getElementById('popUp').style.display = 'none'"><img src="images/xis.png"></div>
			</div>
			<a href="https://loja.cervejariacampinas.com.br/?utm_source=banner-pop-up&utm_medium=search&utm_campaign=site-oficial" target="_blank">
				<div class="imagem-popup">
					<img class="desktop" src="images/banner-desktop.jpg"	 alt="Aviso Cervejaria Campinas" title="Aviso Cervejaria Campinas">
					<img class="mobile" src="images/banner-celular.jpg" alt="Aviso Cervejaria Campinas" title="Aviso Cervejaria Campinas">
				</div>
			</a>
		</div>

	</div>

	<?php include "footer.php"; ?>

	<script src="https://cdn.jsdelivr.net/jquery.cookie/1.4.1/jquery.cookie.min.js"></script>


</body>

</html>
