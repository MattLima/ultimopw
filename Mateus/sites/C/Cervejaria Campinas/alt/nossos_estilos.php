<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">

<head>
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PLKFN6S');
	</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92790401-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-92790401-2');
	</script>


	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, user-scalable=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<title>Nossos Estilos :: Cervejaria Campinas</title>

	<meta property="og:url" content="" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Cervejaria Campinas" />
	<meta property="og:description" content="" />
	<meta property="og:image" content="images/logo-cervejaria-topo.png" />

	<link rel="stylesheet" href="css/style.css" type="text/css" />

	<style>
		.top {
			display: none !important
		}

		.top.dois {
			display: block !important
		}


		.top.dois a {
			width: 100px;
			height: 50px;
			display: flex;
			justify-content: center;
			align-items: center;
			background: #000000;
			color: #ffac4f;
			text-decoration: none;
			position: fixed;
			bottom: 0;
			right: 0;
		}
	</style>
</head>

<body class="dark">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PLKFN6S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php


	function SendSendGrid($from_name, $from_email, $to, $message, $subject, $reply_to)
	{

		$url = 'https://api.sendgrid.com/';
		$user = 'me@hagens.com.br';
		$pass = 'weareH@gens2017';

		$params = array(
			'api_user' => $user,
			'api_key' => $pass,
			'to' => $to,
			'subject' => $subject,
			'html' => $message,
			'from' => $from_email,
			'fromname' => $from_name,
			'replyto' => $reply_to
		);

		$request = $url . 'api/mail.send.json';

		// Generate curl request
		$session = curl_init($request);

		// Tell curl to use HTTP POST
		curl_setopt($session, CURLOPT_POST, true);

		// Tell curl that this is the body of the POST
		curl_setopt($session, CURLOPT_POSTFIELDS, $params);

		// Tell curl not to return headers, but do return the response
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

		// obtain response
		$result = curl_exec($session);
		curl_close($session);

		//error_log('SENDGRID:' . var_export($result, true));

		return $result;
	}


	//verifica se foi postado o form
	if ($_POST['inserirfalk'] == 'true') {
		foreach (array_keys($_POST) as $p) {
			$$p = $_POST[$p];
		}
		//montando o email
		$subject = "Contato Site Cervejaria Campinas - " . date("d/m/Y");

		$message = '
			<html>
			<head>
			<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
			 <title>' . $subject . '</title>
			</head>
			<body>
			<br><font face="Verdana, Arial, Helvetica, sans-serif" size="3" color="#000066"><b>' . $subject . '</b><br><hr><br><br>
			<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
			<b>Nome:</b> ' . $nome . '<br><br>
			<b>e-mail:</b> ' . $email . '<br><br>
			<b>Mensagem:</b> ' . str_replace(chr(10), '<br />', $itensDesejados) . '<br><br>

			</font>
			</body>
			</html>';



		try {
			//    SendSendGrid('Site Cervejaria', 'me@hagens.com.br', 'ralph@hagens.com.br', $message, $subject, $email);
			SendSendGrid('Site Cervejaria', 'me@hagens.com.br', 'marketing@cervejariacampinas.com.br', $message, $subject, $email);
			echo "<script>alert('Sua mensagem foi enviada corretamente! Em breve entraremos em contato.');</script>";
			//echo "<script>window.location='".$linksite."';</script>";

		} catch (Exception $e) {
			echo $e->getCode() . "\n";
			foreach ($e->getErrors() as $er) {
				echo $er;
			}
		}
	}
	?>

	<?php $menu = 'estilos';
	include "header.php"; ?>

	<section class="content slide">
		<div class="middle">
			<h1 class="title-h1">Conheça as nossas cervejas artesanais</h1>
		</div>
	</section>
	<section class="content estilos-internal">
		<div class="middle">
			<h2 class="title-h2">Fique por dentro dos nosso tipos de cerveja artesanal e surpreenda-se</h2>
		</div>
	</section>
	<section class="content estilos-internal" id="topodois">
		<div class="icones">
			<a href="#pilsen">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0010_CAMPINAS-PILSEN.png" alt="PILSEN" title="PILSEN">
				</div>
			</a>
			<a href="#weizen">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0011_LEGIONARIA-WEIZEN.png" alt="LEGIONÁRIA WEIZEN" title="LEGIONÁRIA WEIZEN">
				</div>
			</a>
			<a href="#oatmelstout">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0008_ANDARILHA-OATMEAL-STOUT.png" alt="ANDARILHA OATMEAL STOUT" title="ANDARILHA OATMEAL STOUT">
				</div>
			</a>
			<a href="#hoplager">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0006_HOP-LAGER.png" alt="HOP LAGER" title="HOP LAGER">
				</div>
			</a>
			<a href="#americanwheat">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0014_AMERCICAN-WHEAT.png" alt="AMERICAN WHEAT" title="AMERICAN WHEAT">
				</div>
			</a>
			<a href="#amberale">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0009_AMBER-ALE.png" alt="AMBER ALE" title="AMBER ALE">
				</div>
			</a>
			<a href="#americanipa">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0013_FORASTEIRA-IPA.png" alt="FORASTEIRA IPA" title="FORASTEIRA IPA">
				</div>
			</a>
			<a href="#eldoradopunchipa">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0007_ELDORADO-PUNCH-IPA.png" alt="ELDORADO PUNCH IPA" title="ELDORADO PUNCH IPA">
				</div>
			</a>
			<a href="#tankipa">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0012_TANK-IPA.png" alt="TANK IPA" title="TANK IPA">
				</div>
			</a>
			<a href="#silvestresour">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0004_SILVESTRE-SOUR.png" alt="SILVESTRE SOUR" title="SILVESTRE SOUR">
				</div>
			</a>
			<a href="#gose">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0003_TOO-MUCH-GOSE.png" alt="TOO MUCH GOSE" title="TOO MUCH GOSE">
				</div>
			</a>
			<a href="#ipatododia">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0005_IPATODODIA.png" alt="IPATODODIA" title="IPATODODIA">
				</div>
			</a>
			<a href="#ninetan">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0001_NINE-TAN.png" alt="NINE TAN" title="NINE TAN">
				</div>
			</a>
			<a href="#matrioska">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0002_MATRIOSKA.png" alt="MATRIOSKA" title="MATRIOSKA">
				</div>
			</a>
			<a href="#videnz">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0000_VIDENZ.png" alt="VIDENZ" title="TOO MUCH GOSE">
				</div>
			</a>

			<a href="#ipaze">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0000_IPA_ZERO.png" alt="IPA ZERO" title="IPA ZERO">
				</div>
			</a>

			<a href="#azzapa">
				<div class="icones-img">
					<img src="images/icones/avatar_estilos_CC_0001_AZZAPA.png" alt="AZZAPA" title="AZZAPA">
				</div>
			</a>

		</div>
	</section>

	<section class="content style internal-style">
		<div class="middle">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12" id="01">
					<article class="item" id="pilsen">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-campinas-pilsen-Cervejaria-Campinas.jpg" alt="German PILSEN">
						</div>
						<div class="description">
							<h2 class="title-h2">German Pilsen</h2>
							<h3 class="title-h3">
								15 IBU<br>
								4,7% ABV
							</h3>
						</div>
						<div class="text">
							<p>CAMPINAS Pilsen - estilo tradicional alemão adequado ao paladar do brasileiro. Presença de lúpulos da República Tcheca e Malte tipo Pilsen da Alemanha. O processo de lupulagem é feito com extrema maestria, o qual garante aroma e sabor suave a este estilo.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>Nossa Pilsen não é filtrada o que garante seu diferencial de sabor e corpo.</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Bacalhau, Salmão, Sardinha, Filé de frango, Empanadas, Pizza Marguerita, Salada de folhas verdes.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="02">
					<article class="item" id="weizen">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-legionaria-weizen-Cervejaria-Campinas.jpg" alt="German WEIZEN">
						</div>
						<div class="description">
							<h2 class="title-h2">German Weizen</h2>
							<h3 class="title-h3">15 IBU e 5,5% ABV</h3>
						</div>
						<div class="text">
							<p>CAMPINAS Legionária Weizen - Cerveja LEGIONÁRIA - (cerveja de trigo) - Uma cerveja de trigo proveniente de uma receita tradicional alemã com sabor e aroma pouco mais tendencioso para banana, apesar do cravo ser também perceptível. A acidez comum à esse estilo está bem amenizada reduzindo sua adstringência e aumentando o drinkability. Estes aromas e sabores vêm em grande parte de sua levedura, que é exclusiva desse estilo.</p>

							<h4 class="title-h4">Curiosidade:</h4>

							<p>Os aromas de banana e cravo da weizen vêm de sua levedura que é exclusiva deste estilo</p>

							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Atum, Bolinho de Bacalhau, Massa ao molho de frutos do Mar, Ovos Mexidos e Omeletes, Salmão Defumado, Sushi, Tempurá de vegetais e frutos do mar</p>
						</div>

					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12" id="03">
					<article class="item" id="oatmelstout">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-andarilha_oatmeal_stout_Cervejaria-Campinas.jpg" alt="English Oatmeal STOUT">
						</div>
						<div class="description">
							<h2 class="title-h2">English Oatmeal Stout</h2>
							<h3 class="title-h3">26 IBU e 6,0% ABV</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Andarilha Oatmeal Stout - Cerveja ANDARILHA receita legítima inglesa usando lúpulos ingleses de aroma amadeirado e cinco diferentes maltes, além de flocos de aveia para conferir cremosidade. De aroma e gosto remetendo ao café essa cerveja tem baixo amargor e copo médio.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>alguns dos tipos de maltes utilizados tem seu grau de tosta tão elevado que conferem o aroma de café a essa receita</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Sorvete, Tiramisu, Torta de Frutas, Brownie de Chocolate, Cordeiro Grelhado, Feijoada, Pizza de Banana e Brigadeiro.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="04">
					<article class="item" id="hoplager">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-hop-lager-Cervejaria-Campinas.jpg" alt="American HOP LAGER">
						</div>
						<div class="description">
							<h2 class="title-h2">American HOP Lager </h2>
							<h3 class="title-h3">35 IBU e 5,1% ABV</h3>
						</div>

						<div class="text">
							<p>Campinas Hop Lager - a base do estilo é uma Czech Premium Pale Lager, o que seria a clássica Pilsen fabricada na República Tcheca, mas com um lúpulo americano chamado CITRA, que confere aroma cítrico e amargor na medida certa.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>o lúpulo Citra confere aromas que fazem lembrar manga, melão e até pêssego</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Camarão, Caranguejo, Lula frita, Ostras, Salames, Mandioca/Aipim, Grão de Bico, Filé de Frango.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="05">
					<article class="item" id="americanwheat">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-american-wheat-Cervejaria-Campinas.jpg" alt="American WHEAT">
						</div>
						<div class="description">
							<h2 class="title-h2">American Wheat</h2>
							<h3 class="title-h3">25 IBU e 5,0% ABV</h3>
						</div>

						<div class="text">
							<p>CAMPINAS American Wheat - uma cerveja de trigo sim! Mas não é a cerveja de trigo alemã (Weizen) com aromas de banana de cravo com corpo acentuado; nem as de trigo belgas (Witbier) caracterizada por suas especiarias. A american wheat como toda boa cerveja da escola americana tem o lúpulo como ator principal. O lúpuplo Sorcahi Ace confere aroma e sabor de limão com notas de chá garantindo então a refrescância esperada para este estilo.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>o lúpulo Sorachi Ace foi desenvolvido por uma cervejaria japonesa na década de 70</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Comida Japonesa, Ceasar Salad, Quiche de Queijo, Salmão Grelhado.</p>
						</div>

					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12" id="06">
					<article class="item" id="amberale">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-amber-ale-Cervejaria-Campinas.jpg" alt="American AMBER ALE">
						</div>
						<div class="description">
							<h2 class="title-h2">American Amber Ale</h2>
							<h3 class="title-h3">35 IBU e 5,3% ABV</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Amber Ale - notas de caramelo muito presentes devido aos maltes ingleses com lúpulos americanos conferindo aroma cítrico, mas sem sobrepor a presença de malte. Estilo muito apreciado na Europa sendo um dos estilos mais conhecidos entre as cervejas do tipo ALE.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>a Amber Ale é um estilo muito próximo ao estilo Red Ale, portanto amantes das Irish Red Ale e Red Ale podem experimentar esse estilo que tem grandes semelhanças, mas com característica própria</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Hamburger, Churrasco Brasileiro, Bife Grelhado, Cordeiro Grelhado, Frango Assado, Frango a passarinho.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="07">
					<article class="item" id="americanipa">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-forasteira-ipa-Cervejaria-Campinas.jpg" alt="American IPA">
						</div>
						<div class="description">
							<h2 class="title-h2">American IPA</h2>
							<h3 class="title-h3">50 IBU e 6,4% ABV</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Forasteira IPA - Cerveja FORASTEIRA IPA - Estilo American IPA que tem característico o aroma cítrico fazendo lembrar maracujá ao olfato devido ao lúpulo Amarillo e Galaxy entre outros tipos de lúpulo. Equilíbrio preponderante entre o amargor dos lúpulos e dulçor do malte base Pale Ale. </p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>o processo que garante maior aroma à esse estilo é o Dry Hop, onde se aplica mais lúpulo durante o processo de maturação e não somente durante a brassagem.</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Costela na brasa, Carne Assada, Filet Mignon, Hamburger, Pizza Calabresa.</p>
						</div>

					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12" id="08">
					<article class="item">
						<div class="image">
							<img src="images/barris/barril-esb.jpg" alt="English ESB">
						</div>
						<div class="description">
							<h2 class="title-h2">ESB</h2>
							<h3 class="title-h3">45 IBU e 5,7% ABV</h3>
						</div>

						<div class="text">
							<p>CAMPINAS English Special Bitter - ESB - Campinas #RealityBeerShow ESB - uma receita clássica inglesa muito consumida nos PUBs ingleses e nos principais Pubs do Brasil. Com aparência acobreada e baixa carbonatação essa ESB te remete no aroma a Whisky e Bourbon pelo fato de ter sido adicionado madeira de barril de Carvalho americano durante a maturação. Aromas e sabores mais evidentes acima dos 5º Celsius.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>A ESB da Cervejaria Campinas tem uma história bem legal! Em 2016 a Cervejaria realizou uma competição que foi chamada de #RealityBeerShow, onde convidamos 04 cervejeiros caseiros com seus equipamentos a fazerem uma cerveja junto a 04 pessoas sem nenhuma experiência no mundo da cerveja. A proposta era seguir uma receita base de ESB e adicionar algum ingrediente especial. As duplas utilizaram: gengibre, cardamomo, rapadura e carvalho, sendo a receita que levou carvalho a preferida dos nossos avaliadores e, portanto, a vencedora do reality. Agora em 2017 a Cervejaria Campinas lançou uma edição comemorativa do #RealityBeerShow em lata! Ela tem a assinatura do cervejeiro caseiro e seu parceiro nesse projeto. Saboreie essa cerveja! </p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Carne Assada, Coelho, Carpaccio, Kebab de Carne.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="09">
					<article class="item" id="eldoradopunchipa">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-eldorado-punch-ipa-Cervejaria-Campinas.jpg" alt="Eldorado PUNCH IPA">
						</div>
						<div class="description">
							<h2 class="title-h2">American IPA</h2>
							<h3 class="title-h3">7,5% ABV e 70 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Eldorado PUNCH IPA - Uma porrada nos seu sentidos! Essa é a Eldorado PUNCH IPA – cheia de aromas, sabores e amargor. Essa cerveja da Cervejaria Campinas apresenta uma cor bem dourada e é considerada forte, tanto em amargor quanto no teor alcóolico, mas, ao mesmo tempo é aromática com notas de frutas amarelas como pera, pêssego, nectaria e melão.</p>

							<p>Uma cerveja que trará um soco de amargor, sabor e intensidade devido seus 70 IBU e aromas de frutas amarelas como: melão, abacaxi e nectarina.</p>

							<p>Uma IPA dourada na cor para remeter ao ELDORADO da corrida do ouro. Com Eldorado como lúpulo principal, além de Warrior e Yellow Sub esta cerveja traz notas cítricas muito interessantes além de um sabor único.</p>
						</div>

					</article>
				</div>
				<!--  -->
				<div class="col-md-6 col-sm-6 col-xs-12" id="10">
					<article class="item" id="tankipa">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-imperial-tank-ipa-Cervejaria-Campinas.jpg" alt="Imperial TANK IPA">
						</div>
						<div class="description">
							<h2 class="title-h2">Double IPA</h2>
							<h3 class="title-h3">8,5% ABV e 85 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Imperial TANK IPA - Uma IPA mais robusta, uma verdadeira batalha entre um amargor ALTO ou ABSURDAMENTE ALTO. Vai ser o seu paladar que vai decidir quem vence essa batalha. Essa, então, é a Imperial TANK IPA que a Cervejaria Campinas traz orgulhosamente para você, passando por cima de tudo do que se pensa sobre um estilo forte. Ainda assim é uma cerveja refrescante que na boca irá proporcionar experiências e sensações únicas.</p>
							<p>A TANK é nossa Forasteira IPA ao extremo! Basicamente a mesma base de maltes e lúpulos, mas bem mais intensa! Aroma cítrico remetendo ao maracujá, acobreada na cor e corpo elevado. Uma cerveja intensa, mas sem perder o equilíbrio e drinkability.</p>
						</div>

					</article>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" id="11">
					<article class="item" id="silvestresour">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-silvestre-sour-Cervejaria-Campinas.jpg" alt="Silvestre Sour">
						</div>
						<div class="description">
							<h2 class="title-h2">Catharina Sour</h2>
							<h3 class="title-h3">4% ABV e 5 IBU</h3>
						</div>

						<div class="text">
							<p>Estilo: Catharina Sour ou Sour Ale com frutas</p>
							<p>CAMPINAS Silvestre Sour - Sour Ale é um estilo de cerveja ácida, portanto seu PH é bem baixo. As frutas e também malte de trigo tem função de suavizar além de garantir refrescância e sabor. Silvestre Sour pode ser uma cerveja sazonal até nas frutas numa combinação entre amora, framboesa, mirtilo e morango. Experimente o diferente.</p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="12">
					<article class="item" id="gose">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-too-much-gose-Cervejaria-Campinas.jpg" alt="Silvestre Sour">
						</div>
						<div class="description">
							<h2 class="title-h2">Gose</h2>
							<h3 class="title-h3">5% ABV e 10 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS TOO MUCH Gose - que estilo é esse? Como se fala? "Guuze"? Não, a pronúncia é "góze" mesmo! Esse é um estilo que deixa a cerveja bem ácida e tem adição de sal! Sal? Sim, sal marinho. Na receita também juntamos black pepper e tomate! Para quem é de Minas ou Rio de Janeiro o tomate é falado "túmatchi" daí nossa inspiração para batizá-la Too Much Gose. Uma receita bem peculiar com notas e sensações de condimentos e temperos. Saúde! </p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="13">
					<article class="item" id="ipatododia">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-ipatododia-Cervejaria-Campinas.jpg" alt="IPA Todo Dia">
						</div>
						<div class="description">
							<h2 class="title-h2">Session IPA</h2>
							<h3 class="title-h3">4,4% ABV e 40 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS IPA Todo Dia - Derivada do estilo IPA e com elevadíssimo drinkability a Session IPA tem teor alcoólico reduzido porém sem perder o amargor e aroma cítrico que são característicos da escola americana.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>Session IPA virou um estilo vindo de uma derivação do estilo IPA, mas com teor alcóolico reduzido sem perder as características de amargor e aroma cítrico.</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Mix de castanhas salgadas, batata frita, queijo gorgonzola, culinária mexicana, linguiças apimentadas,carne vermelha, hamburguer.</p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="14">
					<article class="item">
						<div class="image">
							<img src="images/barris/barril-dos-toros.jpg" alt="Dos Toros">
						</div>
						<div class="description">
							<h2 class="title-h2">Double Amber Ale</h2>
							<h3 class="title-h3">7,3% ABV e 70 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Dos Toros - Uma receita extrama da nossa premiada Campinas Amber Ale. Aroma cítrico bem presente, gosto de lúpulo e uma base de malte muito rica. O que nossa Amber Ale puxa mais para o malte a Dos Toros (Double Amber Ale) puxa mais para o amargor dobrado do lúpulo.</p>
							<h4 class="title-h4">Curiosidade:</h4>
							<p>O Estilo Double amber Ale oficialmente não existe. Para criar a Dos Toros usamos como base a recita da nossa cerveja mais premiada, a Campinas Amber Ale. Daí nasceu a Double Amber Ale. Oficialmente a Campinas Dos Toros está dentro do estilo Double Hop Amber Ale. Mas para nós, sempre Double Amber Ale!</p>
							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Amêndoas defumadas, salada verde com queijo de cabra, rosbife, carnes vermelhas grelhadas.</p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="15">
					<article class="item" id="ninetan">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-ipa-9nine-tan-Cervejaria-Campinas.jpg" alt="Triple Ipa">
						</div>
						<div class="description">
							<h2 class="title-h2">Triple Ipa</h2>
							<h3 class="title-h3">9,1% ABV e 100 IBU</h3>
						</div>

						<div class="text">
							<p>Campinas 9Nine TAN Triple IPA - é nossa primeira Triple IPA com 9,1% de ABV e 100 IBU de amargor além de mais de 15 quilos de Tangerina na maturação. Dry Hop de Sabro, El Dorado e Equinox para realçar aromas de tangerina.</p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="16">
					<article class="item" id="matrioska">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-matrioska-Cervejaria-Campinas.jpg" alt="Russian Imperial Stout">
						</div>
						<div class="description">
							<h2 class="title-h2">Russian Imperial Stout</h2>
							<h3 class="title-h3">11% ABV e 70 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Matrioska Russian Imperial Stout - Maturada em barril de AMBURANA. A Matrioska contém 5 maltes diferentes, mais flocos de aveia e cevada torrada. Adição de nibs de cacau e cumaru durante a maturação e assim conferindo notas de chocolate, biscoito, caramelo, café, baunilha e avelã. Intensa desde o aroma até o último gole.</p>
							<p>História do estilo tem a ver com a Rússia, pois foi uma cerveja inglesa adorada por uma rainha chamada Catarina a Grande no final do século 18. A Matrioska foi feita especialmente para a Festa de 3 anos da CAMPINAS em 2019.</p>
						</div>

					</article>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12" id="17">
					<article class="item" id="videnz">
						<div class="image">
							<img src="images/barris/cerveja-artesanal-barril-videnz-Cervejaria-Campinas.jpg" alt="Russian Imperial Stout">
						</div>
						<div class="description">
							<h2 class="title-h2">Hazy IPA</h2>
							<h3 class="title-h3">6,3% ABV e 30 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS Videnz Hazy IPA - Medalha de PRATA na COPA POA 2019. New England ou Hazy IPA é o estilo, demos a ela o nome de Videnz: amarela e turva, devido à grande quantidade de lúpulo com 03 adições no DRY HOP, além de flocos de aveia. Seu aroma é muito intenso e isso é devido ao triplo DRY HOP, feito em grandes quantidades dos lúpulos CITRA, MOSAIC e SIMCOE. Maior parte do lúpulo foi utilizado no dry hop e por isso gosto e aroma intenso de lúpulo. Baixo amargor.</p>
							<p>Feita especialmente para a Festa de 3 anos da CAMPINAS.</p>
						</div>

					</article>
				</div>



				<div class="col-md-6 col-sm-6 col-xs-12" id="18">
					<article class="item" id="ipazero">
						<div class="image">
							<img src="images/barris/ipa-zero-cerveja-artesanal-barril-campinas.jpg" alt="IPA ZERO">
						</div>

						<div class="description">
							<h2 class="title-h2">IPA Zero</h2>
							<h3 class="title-h3">0,4% ABV e 40 IBU</h3>
						</div>

						<div class="text">
							<p>CAMPINAS IPA Zero é uma cerveja sem álcool, de baixíssima caloria e muito prazerosa ao se beber. Seu teor alcoólico é inferior à 0,4% devido a um processo fermentativo muito especial. O aroma cítrico e intenso característico das IPAs vem dos lúpulos americanos aplicados por 3 vezes na maturação pelo processo de Dry Hop. Cerveja ideal para quem deseja/precisa de teor alcoólico e calórico reduzidos! Experimente esse novo sabor.</p>

							<h4 class="title-h4">Curiosidade:</h4>
							<p>Apenas 68 calorias por long neck</p>

							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Costela na brasa, Carne Assada, Filet Mignon, Hamburger, Pizza Calabresa.</p>
						</div>

					</article>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12" id="19">
					<article class="item" id="azzapa">
						<div class="image">
							<img src="images/barris/azzapa-cerveja-artesanal-barril-campinas.jpg" alt="AZZAPA">
						</div>

						<div class="description">
							<h2 class="title-h2">AZZAPA - APA (American Pale Ale)</h2>
							<h3 class="title-h3"> 38 IBU e 5,2% ABV </h3>
						</div>

						<div class="text">
							<p>Em nossa primeira single hop utilizamos apenas 1 variedade de lúpulo, tanto para o amargor como para o Dry Hop, o que garante um aroma mais intenso e bem mais cítrico. O lúpulo utilizado é o Azzaca, que inspirou o nome e a cara dessa nossa cerveja. Usamos 5 maltes diferentes para conseguir a cor e o sabor esperado para uma American Pale Ale. Não tão alcóolica, nem amarga ou tão aromática quanto uma American IPA, mas é uma boa cerveja de entrada no mundo das artesanais.</p>

							<h4 class="title-h4">Curiosidade:</h4>
							<p>Corpo leve, seca, de aroma cítrico leve remetendo mais à frutas tropicais, com notas de manga, pêra e maçã.</p>

							<h4 class="title-h4">Sugestão de harmonização:</h4>
							<p>Hot dog, tacos, cordeiro, torresmo, peru e churrasco.</p>
						</div>

					</article>
				</div>


			</div>
		</div>



	</section>
	<!-- Modal -->
	<div class="modal fade" id="modalBarris" tabindex="-1" role="dialog" aria-labelledby="modalBarris" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h2 class="title-h2">Consulta de Valores</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<div class="text">
								<strong>Todos nossos itens estão a venda em nossa Tap House:</strong>
								<p>Rua Paula Bueno, 664 Taquaral, Campinas/SP</p>
								<p>(19) 3239-6742</p>
							</div>
							<form class="form-horizontal" id="form-validate" method="post" action="" enctype="multipart/form-data">
								<div class="text">
									<strong>Para consultar os valores, envie-nos uma mensagem:</strong>
								</div>
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="nome">Seu nome:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<input type="text" name="nome" id="nome" required>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="email">Seu e-mail:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<input type="email" name="email" id="email" required>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<label for="itensDesejados">Itens desejados:</label>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<textarea name="itensDesejados" id="itensDesejados" required class="form-control textarea-form" placeholder="Olá, gostaria dos valores dos seguintes itens:" rows="3">Olá, gostaria dos valores dos seguintes itens:</textarea>

									</div>
									<div class="col-md-4 col-sm-4"></div>
									<div class="col-md-8 col-sm-8 col-xs-12">
										<button type="submit" class="btn btn-lg btn-primary btn-enviar">Consultar preços</button>
										<input name="inserirfalk" type="hidden" id="inserirfalk" value="true" />
									</div>
								</div>
						</div>
						<!--  -->
						<div class="col-md-5 col-sm-5 col-xs-12">
							<div class="image full"><img src="images/barris.jpg" alt="Barris"></div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="top dois">
		<a href="#topodois"><i class="fas fa-arrow-alt-circle-up"></i> TOPO</a>
	</div> <!-- top -->

	<?php include "footer.php"; ?>

</body>

</html>