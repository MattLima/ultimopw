<?php
$dadosPagina["titulo"]   = "Esquadrias em Alumínio – Techno Alum";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Empresa do segmento de esquadrias em alumínio, revestimentos em ACM e fachadas em pele de vidro. Atuação desde a venda até a entrega final do produto, residencial ou comercial.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Esquadrias em Alumínio – Techno Alum\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>

<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <li><img src="[template]/pw-slider-data/images/empresa-referencia-em-esquadrias-de-aluminio.jpg" alt="Empresa Referência em Esquadrias de Alumínio" title="Empresa Referência em Esquadrias de Alumínio" id="wows1_0" /></li>
            <li><img src="[template]/pw-slider-data/images/esquadrias-de-aluminio-em-campinas-sp.jpg" alt="Esquadrias de Alumínio em Campinas - SP" title="Esquadrias de Alumínio em Campinas - SP" id="wows1_1" /></li>
        </ul>
    </div>

    <div class="ws_shadow"></div>
</div>
<!-- End WOWSlider.com BODY section -->


<div class="box-01-total">


    <div class="box-01">

        <div class="conteudo">
            <div class="titulo">SEJA BEM-VINDO AO<br />NOSSO SITE</div> <!-- Titulo -->
            <div class="linha">
                <div class="barra"></div>
            </div> <!-- Linha -->
            <div class="texto">
                <p> Realizamos seu projeto de acordo com sua necessidade, para isso contamos com uma equipe totalmente qualificada e experiente, além de trabalharmos com matéria prima de primeira linha. Dando um aspecto moderno e sofisticado seja nas Esquadrias em Alumínio, Fachada Pele de Vidro ou Revestimento em ACM. </p>
            </div> <!-- Texto -->

            <a class="entrar" href="[url]/empresa-referencia-em-esquadrias-de-aluminio" title="Empresa Referência em Esquadrias de Alumínio" data-icon="empresa">
                <div class="entrar">
                    <p> Saiba mais </p>
                </div> <!-- Entrar -->
            </a>

        </div> <!-- Conteudo -->
        <div class="img">
            <img src="[template]/pw-images/empresa-referencia-em-esquadrias-de-aluminio.jpg" alt="Empresa Referência em Esquadrias de Alumínio" title="Empresa Referência em Esquadrias de Alumínio" />
            <div class="info"><i class="far fa-thumbs-up"></i><br />" Atender Bem Para Atender Sempre"</div> <!-- Info -->
        </div> <!-- Img -->

    </div> <!-- Box 01 -->


</div> <!-- conteudo index -->


<div class="box-02-total">

    <div class="box-02">

        
            <div class="item">

                <div class="img"><img src="[template]/pw-images/esquadrias-em-aluminio-campinas-sp.jpg" alt="Esquadrias de Alumínio em Campinas - SP" title="Esquadrias de Alumínio em Campinas - SP" /></div> <!-- Img -->
                <div class="titulo">ESQUADRIAS EM ALUMÍNIO</div> <!-- Titulo -->
                <div class="conteudo">
                    <p> Realizada sob medida, as Esquadrias em Alumínio será um destaque no projeto. Oferece ao cliente a possibilidade em cores, modernidade, durabilidade dentre outros benefícios. </p> 
                </div> <!-- Conteudo -->

                <a class="conteudo" href="[url]/esquadrias-em-aluminio" title="Serviços de Esquadrias em Alumínio – Techno Alum" data-icon="empresa">
                <div class="entrar">Ver Mais</div> <!-- Entrar -->
                </a>

            </div> <!-- Item -->
        
        
            <div class="item">

                <div class="img"><img src="[template]/pw-images/revestimento-em-acm-e-brises-campinas-sp.jpg" alt="Revestimento em ACM e Brises Campinas SP" title="Revestimento em ACM e Brises Campinas SP" /></div> <!-- Img -->
                <div class="titulo">REVESTIMENTO EM ACM E BRISES</div> <!-- Titulo -->

                <div class="conteudo">
                    O uso do ACM devido a sua flexibilidade é ideal para realizar projetos que contemplem formas, raios e ângulos. Conhecido como revestimento do futuro, o ACM é muito utilizado para comunicação visual, e fará da sua fachada um cartão de visita.
                </div> <!-- Conteudo -->

                <a class="conteudo" href="[url]/revestimento-em-acm-e-brises" title="Serviços de Revestimento em ACM e Brises – Techno Alum" data-icon="empresa">
                <div class="entrar">Ver Mais</div> <!-- Entrar -->
                </a>
            </div> <!-- Item -->
        
        
            <div class="item">

                <div class="img"><img src="[template]/pw-images/fachada-pele-de-vidro-campinas-sp.jpg" alt="Fachada Pele de Vidro Campinas - SP" title="Fachada Pele de Vidro Campinas - SP" /></div> <!-- Img -->
                <div class="titulo">FACHADA PELE DE<br> VIDRO</div> <!-- Titulo -->
                <div class="conteudo">
                    Tendência na arquitetura mundial as Fachadas Pele de Vidro além da beleza, traz vantagens como: Luminosidade (de 100% de passagem de luz até um efeito Black-out), redução de ruídos externos, versatilidade de cores. É um conjunto ideal para um projeto moderno e versátil. Oferecemos para nossos clientes um leque de possibilidades.
                </div> <!-- Conteudo -->
                <a class="conteudo" href="[url]/fachada-pele-de-vidro" title="Serviços de Fachada Pele de Vidro" data-icon="empresa">
                <div class="entrar">Ver Mais</div> <!-- Entrar -->
                </a>

            </div> <!-- Item -->
        

    </div> <!-- BOx 02 -->

</div> <!-- Box 02 Total -->

<div class="box-03-total">

    <div class="box-03">

        <div class="linha">
            <div class="barra"></div>
        </div> <!-- Linha -->
        <div class="titulo">ENTRE EM <br />CONTATO</div> <!-- Titulo -->

        <form action="pw-form.php" method="post">
            <div class="item">
                <input name="campo[Nome]" type="text" placeholder="Nome:" />
                <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
            </div> <!-- item -->
            <div class="item">
                <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
            </div> <!-- item -->
            <input class="submit" type="submit" value="Enviar" />
        </form>

    </div> <!-- Box 03 -->

</div> <!-- Conteudo Pages -->

<div class="mapa">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14692.051833217994!2d-47.10685772869562!3d-22.9865509700737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8ca261dd41e49%3A0xd618b2ab370dad29!2sR.%20Eldorado%2C%20926-960%20-%20Parque%20Sao%20Paulo%2C%20Campinas%20-%20SP%2C%2013052-450!5e0!3m2!1spt-BR!2sbr!4v1597426543610!5m2!1spt-BR!2sbr" allowfullscreen></iframe>

</div> <!-- mapa -->