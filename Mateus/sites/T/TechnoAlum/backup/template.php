<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]
    <title>[pagina]</title>
    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <style><?php echo file_get_contents('template/pw-css/style.css');?></style>
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">
    <link rel="stylesheet" href="pw-galeria-fancybox/jquery.fancybox.min.css">
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }

    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163937866-26"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163937866-26');
</script>
</head>

<body>



    <div class="linha-total">

        <div class="linha">
            <div class="item"><i class="fas fa-phone-square"></i> (19) 2144-6527</div>
            <!-- Item -->
            <div class="item"><i class="fas fa-envelope-open-text"></i> contato@technoalum.com.br </div>
            <!-- Item -->
        </div>
        <!-- linha -->

    </div>
    <!-- Linha Total -->

    <div class="topo-total">

        <div class="topo">

            <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="Esquadrias em Alumínio – Techno Alum" title="Esquadrias em Alumínio – Techno Alum" /></a></div>
            <!-- Logo -->

            <div class="menu">

                <ul>
                    <li><a href="[url]/" title="Esquadrias em Alumínio – Techno Alum">HOME</a></li>
                    <li><a href="[url]/empresa-referencia-em-esquadrias-de-aluminio" title="Empresa Referência em Esquadrias de Alumínio">EMPRESA</a> <!-- [url]/ -->
                    <li><a href="[url]/servicos-residenciais-ou-comercias-em-esquadrias-de-aluminio" title="Esquadrias em Alumínio, Revestimentos em ACM e Fachadas em Pele de Vidro">PORTFÓLIO</a></li>
                    <li><a href="[url]/qualidade-em-esquadrias-em-aluminio" title="Qualidade em Esquadrias em Alumínio">PARCEIROS</a></li>
                    <li><a href="[url]/contato-de-empresa-de-esquadrias-em-aluminio" title="Empresa de Esquadrias em Alumínio, Fachadas">CONTATO</a></li>
                </ul>

            </div>
            <!-- Menu -->

        </div>
        <!-- Topo -->

    </div>
    <!-- Topo Total -->

    <div class="global">

        [conteudo]

    </div>
    <!-- global -->

    <div class="rodape-total">

        <div class="item-rodape-total">
            <div class="rodape">
                <!-- Item -->
                <div class="item"><a href="[url]/" title=""><img src="[template]/pw-images/logo-rodape.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a></div>
                <!-- Item -->

            </div>
            <!-- Rodape -->
            <div class="rodape contatos">
                <p><strong>Contato</strong></p>
                <div class="item-contato">
                <div class="item"><i class="fas fa-phone-volume"></i> (19) 2144-6527</div>
                <!-- Item -->
                <div class="item"><i class="fas fa-envelope-open-text"></i> contato@technoalum.com.br </div>
                <!-- Item -->
                <div class="item"><i class="fas fa-map-marker-alt"></i> Rua Eldorado N° 960, Parque São Paulo CEP 13052-470 Campinas SP</div>
                <!-- Item -->
                </div>
            </div>
        </div>
        <!-- Rodape -->
        <div class="logo-pw-total">
            <div class="logo-pw">
                <div class="item-logo-pw">
                    <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
                    <div>
                        <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                        <p>
                            <a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
                            <a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites</span></a>
                            <a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site</span></a>
                            <a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site</span></a>
                            <a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites</span></a>
                            <a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas</span></a>
                            <a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="faixa-rodape" style="display: none;">
            <?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php ');?>
        </div>



    </div>
    <!-- Rodape Total -->

    <script>
        <?php echo file_get_contents('pw-js/jquery.js'); ?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/scrollReveal.js'); ?>

    </script>
    <script>
        <?php echo file_get_contents('pw-js/javascript.js'); ?>

    </script>

    <script src="[template]/pw-slider-engine/wowslider.js"></script>

    <script src="[template]/pw-slider-engine/script.js"></script>

    <script src="pw-galeria-fancybox/jquery.fancybox.min.js"></script>

    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.box-02 .item', {
                duration: 2000,
                origin: 'right',
                distance: '100px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
            sr.reveal('.box-03 input, .box-03 textarea', {
                duration: 2000,
                origin: 'bottom',
                distance: '50px',
                viewFactor: 0.6,
                mobile: false
            }, 100);
            sr.reveal('.mapa', {
                duration: 2000,
                origin: 'bottom',
                distance: '0px',
                viewFactor: 0.6,
                mobile: false
            }, 300);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }

    </script>
</body>

</html>
