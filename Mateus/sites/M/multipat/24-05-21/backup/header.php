<?php session_start();
if ($_SESSION['enviado'] == 1) {
	echo "<script>alert('Sua mensagem foi enviada com sucesso!')</script>";
	unset($_SESSION['enviado']);
} ?>
<!DOCTYPE html>
<html>

<head>
	<title><?php wp_title(''); ?></title>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<link href='<?php bloginfo('template_directory'); ?>/favicon.png' rel='shortcut icon' type='image/x-icon' />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/pw-font-awesome/css/all.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/pw-slider-engine/style.css">
	<!-- CSS -->

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
	<?php wp_head(); ?>

</head>

<body>

	<div class="linha-total">

		<div class="linha">
			<div class="item"><i class="fas fa-phone-square"></i> (19) 3236-0111 | (19) 3256-3797 </div>

			<div class="item"><i class="fas fa-envelope-open-text"></i> sac@multipat.com.br </div>
		</div>

	</div>

	<div class="topo-total">

		<div class="topo">

			<div class="logo">
				<a href="<?php echo get_site_url(); ?>" title="">
					<img src="<?php echo get_field('logo', 13); ?>" alt="" title="" />
				</a>
			</div>

			<div class="menu">

				<?php
				wp_nav_menu(array(
					'menu' => 'menu_topo',
					'theme_location' => 'menu_topo',
					'container' => 'div',
					'container_class' => 'classe_do_container',
					'container_id' => 'id_do_container',
					'menu_class' => 'classe_do_menu',
					'echo' => true,
					'menu_id' => 'id_do_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'depth' => 0,
					'walker' => '',
				));
				?>

			</div>

		</div>

		<div class="btnTopo">
			<a href="#">RESULTADOS DE EXAMES</a>
			<a href="#">COMO ACESSAR MEU RESULTADO</a>
		</div>

		

	</div>