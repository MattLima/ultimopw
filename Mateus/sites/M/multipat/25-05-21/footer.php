<?php wp_footer(); ?>

<div class="rodape-total">

	<div class="topo-chat">
		<a target="_blank" href="https://api.whatsapp.com/send?phone=5599454-0862">
			<div class="item whats"><i class="fab fa-whatsapp"></i></div>
		</a>
	</div>

	<div class="item-rodape-total">
		<div class="rodape">
			<div class="item">
				<a href="<?php echo get_site_url(); ?>" title="">
					<img src="<?php echo get_field('logo_rodape', 13); ?>" alt="" title="" />
				</a>
			</div>
		</div>
		
		<div class="rodape contatos">
			<p><strong>Contato</strong></p>
			<div class="item-contato">
				<div class="item"><i class="fas fa-phone-volume"></i> <?php echo get_field('telefone', 13); ?> <br> <?php echo get_field('telefone_2', 13); ?> </div>
				
				<div class="item"><i class="fas fa-envelope-open-text"></i> <?php echo get_field('email', 13); ?></div>
				
				<div class="item"><i class="fas fa-map-marker-alt"></i> <?php echo get_field('endereco', 13); ?></div>	
			</div>
		</div>

	</div>



	<div class="frase-rodape">
		<p>O Multipat possui conexão e comunicação com terceiros, estranhos a este site, que podem conter informações e ferramentas úteis para os visitantes. Não nos responsabilizamos pelo conteúdo disponibilizado em outros locais, de maneira que a nossa política de privacidade não é aplicada a terceiros.</p>
	</div>
	
	<div class="logo-pw-total">
		<div class="logo-pw">
			<div class="item-logo-pw">
				<a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
				<div>
					<p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
					<p>
						<a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
						<a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites</span></a>
						<a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site</span></a>
						<a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site</span></a>
						<a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites</span></a>
						<a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas</span></a>
						<a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="faixa-rodape" style="display: none;">
		<?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php '); ?>
	</div>
</div>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/scrollReveal.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-slider-engine/wowslider.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-slider-engine/script.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/javascript.js"></script>
<script type="text/javascript">
	(function($) {
		'use strict';
		//https://github.com/jlmakes/scrollreveal.js
		//[data-sr="enter direction"]
		//direction = top, bottom, right, left	
		window.sr = ScrollReveal();
		sr.reveal('.box-produtos a', {
			duration: 2000,
			origin: 'bottom',
			distance: '100px',
			viewFactor: 0.6
		}, 100);

		
		
		sr.reveal('.titulo-total h1', {
			duration: 2000,
			origin: 'right',
			distance: '500px',
			viewFactor: 0.6
		}, 100);
		 


	})();


	
	var swiper = new Swiper('.swiper-container', {
		pagination: '',
		slidesPerView: 1,
		slidesPerColumn: 1,
		paginationClickable: true,
		spaceBetween: 0,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		freeMode: false
	});
</script>
</body>

</html>