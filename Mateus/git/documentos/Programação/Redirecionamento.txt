RewriteEngine On
RewriteCond %{HTTP_HOST} ^dominio.com.br
RewriteRule ^ http://www.dominio.com.br%{REQUEST_URI} [L,R=301]

<IfModule mod_rewrite.c>
 RewriteEngine on
 RewriteCond     %{SERVER_PORT} ^80$
 RewriteRule     ^(.*)$ https://%{SERVER_NAME}%{REQUEST_URI} [L,R]
</IfModule>