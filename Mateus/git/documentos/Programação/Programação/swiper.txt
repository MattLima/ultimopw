    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><img src="[template]/pw-images/banners/1.jpg" alt="" title="" /></div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-white"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
    </div>
    <!-- Swiper -->



    var swiper = new Swiper('.swiper-container', {
        pagination: '',
        slidesPerView: 2,
        loop: false,
        autoplay: 5000,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 0,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: false,
        breakpoints: {
            // when window width is >= 320px
            500: {
                slidesPerView: 1,
                slidesPerColumn: 1,
            }
        }
    });