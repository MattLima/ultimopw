<?php get_header(); ?>
 

<div class="conteudo-pages">	
	
		<div class="global">

			<div class="box-banner">
				<div class="texto-banner">
					<h2>Soluções em <br> <span>tratamento de água</span> </h2>
				</div>
			</div>


			<div class="box-01-total">

				<div class="box-01">  

					<div class="box-01-conteudo">
					<h2>SEJA BEM VINDO A AFLUX BRASIL</h2>

					</div>


					<div class="box-01-conteudo">
					<h3>UMA EMPRESA PARA ATENDER SUAS DEMANDAS EM PRODUTOS E SISTEMAS PARA <br> TRATAMENTO DE ÁGUAS E EFLUENTES, MONTAGENS INDUSTRIAIS E CONSTRUÇÃO CIVIL</h3>

					</div>


					<div class="box-01-conteudo">
					<p>A AFLUX BRASIL, mantém em sua equipe profissionais altamente capacitados para atender as demandas de empresas privadas ou públicas, 
						   em Sistemas de Tratamentos de Águas e Efluentes, Montagens Industriais e Construção Civil, nos diversos segmentos industriais.
						   Em nossos projetos priorizamos o uso de tecnologias avançadas e eficazes, para atendermos os níveis de exigências de nossos clientes.</p>

					</div>

				</div>
			
			</div>
			

			<div class="box-02-total">

				<div class="box-02">

					<div class="box-02-titulo">
						<h2>PRINCIPAIS PRODUTOS</h2>
					</div>

					<div class="box-02-linha">
						<hr>
					</div>

					<div class="box-02-subtitulo">
						<p>Os Insumos e Produtos abaixo a AFlux Brasil mantêm Grandes Estoques, para Entregas Imediatas.</p>
					</div>

					<div class="box-02-conteudo">

						<div class="box-02-imagens">

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoCanos.jpg" alt="" title="" />
								<h2 class="subtitulo">Cartuchos de Polipropileno – Melt Blown</h2>
								<p>O Elemento Filtrante em Polipropileno Liso é produzido com 100% de polipropileno atóxico. A matéria-prima é aquecida e extrusada em pequenos filamentos que formam os microporos do elemento filtrante. Esse processo, permite uma filtração altamente eficiente ao longo de toda a vida útil do produto.</p>
							</div>

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoZeolita.jpg" alt="" title="" />
								<h2>Zeólita para Remoção de Ferro e Manganês</h2>
								<p>É um meio filtrante granulado de elevada quantidade de minerais ativos, com alto desempenho para remoção de Ferro e Manganês em Águas Industriais e Potáveis.</p>
							</div>

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoMembrana.jpg" alt="" title="" />
								<h2>Membranas de Osmose Reversa</h2>
								<p>São membranas de fluxo transversal que agem como uma barreira física à todos os sais e moléculas orgânicas e inorgânicas dissolvidas em um solvente, geralmente na água.</p>
							</div>

						</div>

						<div class="box-02-imagens">

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoVasos.jpg" alt="" title="" />
								<h2>Vasos para Osmose Reversa</h2>
								<p>Os Vasos de Pressão para Osmose Reversa são produtos fabricados com as mais Altas Tecnologias, os mesmos são fabricados com Plástico Rígido de Fibra de Vidro (PRFV) e suportam até 1200PSI (83 bar).</p>
							</div>

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoFiltros.jpg" alt="" title="" />
								<h2>Filtros/Tanques de PRFV</h2>
								<p>Os Tanques em PRFV (Plástico Rígido com Fibra de Vidro) são confeccionados com a mais alta tecnologia e materiais de excelente qualidade</p>
							</div>

							<div class="box-imagem-produto">
								<img src="<?php bloginfo('template_directory'); ?>/pw-images/tratamentoValvula.jpg" alt="" title="" />
								<h2>Válvulas Automáticas e Manuais (3 vias) para Filtros e Abrandadores</h2>
								<p>Válvulas 3 vias Manual e Automática, para todos os tipos de filtragem e para resinas.</p>
							</div>
							
						</div>

					</div>

				</div>

			</div>


			<div class="box-banner-dois">

				<div class="banner-dois-conteudo">

					<div class="banner-conteudo-titulo">

							<div class="banner-titulo-titulo">
								<h2>Portfólio</h2>
							</div>

							<div class="box-02-linha">
								<hr>
							</div>

							<div class="imagens-box-banner">

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio2.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio3.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio4.jpg" alt="" title="" />
								</div>
								
							</div>

					</div>
					
				</div>
			</div>


			<div class="box-03-total">
				<div class="box-03">
					<div class="conteudo-03">

						<div class="titulo-03">
							<h2>CONHEÇA UM POUCO MAIS SOBRE A NOSSA EMPRESA</h2>
						</div>

						<div class="texto-03">
							<h3>Na língua chinesa, a palavra crise apresenta o significado, Oportunidade. Foi exatamente através de uma crise, onde o talento, a técnica, o profissionalismo e o “know how” encontraram a oportunidade para realizar além de uma solução, também exercer uma Paixão!</h3>
							<p>Nosso objetivo é que sejam estudadas novas estratégias de uso sustentável da água, frente às necessidades das populações, ecossistemas e uso em atividades produtivas.</p>
							<p>A AFLUX é uma empresa especialista em desenvolver projetos de engenharia para Sistemas de tratamento de água e efluentes, montagens industriais e construção civil. Projeta, fabrica, instala e opera equipamentos com alta performance, que permitem otimizar as características físico químicas e microbiológicas da água.</p>
						</div>
						
					</div>

					<div class="logoBanner">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoBanner.png" alt="" title="" />
					</div>
					
				</div>
			</div>
			

		</div>

</div> <!-- conteudo pages -->






<?php get_footer(); ?>


		