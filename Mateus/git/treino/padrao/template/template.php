<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css');?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css"> 



    <!--  GALERIA  -->
    <link rel="stylesheet" href="pw-fancybox/jquery.fancybox.min.css">



    
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>
</head>

<body>

	<div class="topo-total">
    
    	<div class="topo">
        
        	<!-- <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="" title="" /></a></div>  -->
            
            <!-- Logo -->
        
        </div> <!-- Topo -->
    
    </div> <!-- Topo Total -->
    
    <div class="menu-total">
	
		<div class="menu-resp"></div> <!-- Menu Resp -->
    
    	<div class="menu">
        
        	<ul>
                <li class="logo"><a href=""><img src="[template]/pw-images/logo.png"/></a></li>
                <li><a href="">Link 1</a></li>
                <li><a href="">Link 2</a></li>
                <li><a href="">Link 3</a></li>
                <li><a href="">Link 4</a></li>
            </ul>
        
        </div> <!-- Menu -->
    
    </div> <!-- menu Total -->

<div class="global">

	[conteudo]
	
</div> <!-- global -->

	<div class="rodape-total">
    
    	<div class="rodape">
        
        	<div class="logo-rodape"><a href="[url]/" title="" ><img src="[template]/pw-images/logo.png" alt="" title="" /></a></div> <!-- logo Rodape -->
            
            <!-- <div class="logo-pw">
                <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Projeto Web Site - Agência, Projetos, Marketing, Websites" title="Projeto Web Site - Agência, Projetos, Marketing, Websites" /></a>
                <div>
                    <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                    <p>
                        <a href="https://www.projetowebsite.com.br/" target="_blank"><span>Site/</span></a>
                        <a href="https://www.projetoweb.com.br/criar-site" target="_blank"><span>Criar Site/</span></a>
                        <a href="https://www.projetowebsite.com.br/" target="_blank"><span>Sites</span></a>
                    </p>
                </div>
            </div> -->
        
        </div> <!-- Rodape -->
    
    </div> <!-- Rodape Total -->
	
	<script><?php echo file_get_contents('pw-js/jquery.js');?></script>
    <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
    <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
    <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>


    <!--  GALERIA  -->
    <script><?php echo file_get_contents('pw-fancybox/jquery.fancybox.min.js');?></script>


	<script>
	 
		(function($) {	
			'use strict';
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		  
		  
	</script>
</body>

</html>