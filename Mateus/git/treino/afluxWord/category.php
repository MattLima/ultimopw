<?php get_header(); ?>
<?php $cat_id = get_queried_object_id(); ?>

<div class="titulo-total pages"></div>

<div class="page-blog">

    <div class="titulo">
        <h1> Você está na categoria: <span><?php echo get_cat_name($cat_id); ?></span></h1>
    </div>




    <div class="blog-total">

        <div class="blog">


            <?php $posts = get_posts(array('post_type' => 'post', 'order' => 'ASC', 'numberposts' => -1, 'category' => $cat)); ?>
                        <?php foreach ($posts as $post) { ?>
                            <?php setup_postdata($post); ?>

                            <div class="post">
                                <a href="<?php echo get_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <div class="img-thumb"><?php echo get_the_post_thumbnail(); ?></div>

                                    <div class="text">
                                        <h4><?php echo get_the_title(); ?></h4>
                                        <span><?php echo the_date(); ?></span>
                                        <p><?php echo get_the_excerpt(); ?></p>
                                    </div>
                                </a>
                            </div><!-- post -->

                        <?php } //FECHA FOREACH
                        ?>

    



        
                        
        </div>


    <div class="categorias">
            <h2>Categorias</h2>
                <ul>
                    
                    <?php
                        $categories = get_categories(array(
                            'post_type' => 'post',
                            'orderby' => 'name',
                            'parent'  => 0
                        ));

                        foreach ($categories as $category) {
                            printf(
                                '<a href="%1$s"><li><i class="fas fa-angle-right"></i>%2$s</li></a>',
                                esc_url(get_category_link($category->term_id)),
                                esc_html($category->name)
                            );
                        } ?>

                </ul>
    </div>

</div>




</div>

<?php get_footer(); ?>