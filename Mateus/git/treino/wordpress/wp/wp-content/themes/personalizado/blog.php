<?php /* Template name: Blog */ ?>
<?php get_header(); ?>

<div class="titulo-total pages"></div>

<div class="page-blog">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="titulo">
				<?php the_post_thumbnail(); ?>
				<h1><?php echo get_the_title(); ?></h1>
			</div>

        <?php endwhile; ?>
    <?php endif; ?>

    <section class="blogBody">

        <div class="texto-blog">
            <div class="posts-blog">
                <?php
                // the query to set the posts per page to 3
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array('posts_per_page' => 5, 'paged' => $paged, 'post_type' => 'post');
                query_posts($args); ?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <div class="post">
                            <a href="<?php echo get_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <div class="img-thumb"><?php echo get_the_post_thumbnail(); ?></div>

                                <div class="text">
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <span><?php echo the_date(); ?></span>
                                    <p><?php echo get_the_excerpt(); ?></p>
                                </div>
                            </a>
                        </div><!-- post -->

                    <?php endwhile; ?>
                    <!-- pagination -->

                    <div class="proxima-pagina"><?php next_posts_link(); ?></div>
                    <div class="pagina-anterior"><?php previous_posts_link(); ?></div>
                <?php else : ?>
                    <!-- No posts found -->
                <?php endif; ?>
            </div>

        </div> <!-- Texto -->

        <div class="categorias">

            <div class="box-categorias">
                <div class="titulo-categoria">
                    <h2>CATEGORIAS</h2>
                </div>

                <div class="item-categoria">
                    <?php
                    $categories = get_categories(array(
                        'post_type' => 'post',
                        'orderby' => 'name',
                        'parent'  => 0
                    ));

                    foreach ($categories as $category) {
                        printf(
                            '<a href="%1$s"><li><i class="fas fa-angle-right"></i>%2$s</li></a>',
                            esc_url(get_category_link($category->term_id)),
                            esc_html($category->name)
                        );
                    } ?>
                </div>
            </div>

        </div>

    </section>

</div> <!-- conteudo-pages -->

<?php get_footer(); ?>