<?php get_header(); ?>

<div class="conteudo-pages">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="titulo-total pages"></div>

			<div class="texto pages">
				<div class="titulo">
					<h1><?php echo get_the_title(); ?></h1>
				</div>

				<?php echo the_content(); ?>
			</div>

		<?php endwhile; ?>
	<?php endif; ?>

</div>

<?php get_footer(); ?>