<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 12";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 12\" />";
    $dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">

	<div class="titulo">
		<h1><i class="far fa-building"></i> <br> Sobre Nós</h1>
	</div>

	<div class="box-conteudo">

		<img src="[template]/pw-images/empresa.jpg" alt="">

		<div class="texto">
			<p>Profissional com formação superior em Medicina, título de especialização em Neurocirurgia, pós-graduado (Mestrado) em Medicina pela Faculdade de Medicina da USP (Universidade de São Paulo) e Administração de Sistemas de Saúde, com passagem em diversos hospitais privados e da rede pública.</p>
			<p>Exerci a chefia de equipe médica no Hospital Municipal Dr. Arthur Ribeiro de Saboya e a Coordenação da equipe de Neurocirurgia do Instituto de Infectologia Emílio Ribas.</p>
			<p>Ocupei os cargos de Vice-Presidente da SONESP (Associação de Neurocirurgia do Estado de São Paulo) e de Diretor de Comunicação da SBN (Sociedade Brasileira de Neurocirurgia).</p>
			<p>Internacionalmente, ocupo a posição de membro da Comissão de Afiliação da Sociedade Norte-Americana de Coluna (NASS) desde 2013.</p>
		</div>

	</div>
	
</div>
