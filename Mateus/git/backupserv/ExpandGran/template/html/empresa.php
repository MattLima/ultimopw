<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 12";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 12\" />";
    $dadosPagina["css"] = "<style></style>";
?>

<div class="background">
	<div class="conteudo-pages">


		<div class="titulo">
			<h1>CONHEÇA NOSSA EMPRESA</h1>
		</div>

		<div class="box-empresa">

			<img src="[template]/pw-images/empresa.jpg" alt="">

			<div class="texto">
				<p>
					A Expand Gran Mármores e Granitos atua no mercado de rochas ornamentais, oferecendo a seus clientes
					o
					fornecimento e instalação de materiais nacionais e importados e conta com qualificada
					tecnologia e constante desenvolvimento profissional a fim de prestar um atendimento especializado,
					com
					produtos e mão de obra de qualidade.<br><br>

					Realizamos o beneficiamento de nossos materiais através de um processo úmido onde buscamos preservar
					a
					saúde de nossos colaboradores e melhor qualidade do produto final, além de máquinas e insumos de
					primeira linha. <br><br>

					Solicite um orçamento, conheça nossos produtos e serviços e entenda por que estamos em constante
					expansão.
				</p>
			</div>

		</div>

	</div>
</div>