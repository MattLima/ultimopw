<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 12";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 12\" />";
    $dadosPagina["css"] = "<style></style>";
?>


<div class="background">
	<div class="conteudo-pages">


		<div class="titulo">
			<h1>SAIBA MAIS</h1>
		</div>


		<div class="box-servicos-total">

			<div class="box-servicos">
				<div class="servico">
					<div class="texto-servico">
						<div class="titulo-servico">
							<h2>Tipos de acabamento:</h2>
						</div>
						<p><b>Apicoado:</b> textura porosa e uniforme, adquirido através de impactos.</p>
						<p><b>Bruto:</b> apresenta-se em características naturais, sem acabamento algum.</p>
						<p><b>Flameado:</b> aspecto rugoso e ondulado, feito a base de fogo.</p>
						<p><b>Jateado:</b> aparência opaca, adquiridos a partir de jatos de areia.</p>
						<p><b>Levigado:</b> acabamento semi-polido, lixado com abrasivos.</p>
						<p><b>Polido:</b> aspecto liso e brilhante, feito a partir de lustração em mármores e granitos.
						</p>
					</div>
				</div>
			</div> <!-- Box Serviços -->

		</div> <!-- Box Serviços Total -->

        <div class="box-servicos-total">

<div class="box-servicos">
<h2>Guia de recomendação de uso</h2>
    <div class="tabela">
        <div class="linha-tab">
            <div class="col"><p></p></div>
            <div class="col"><p>Mármore</p></div>
            <div class="col"><p>Travertino</p></div>
            <div class="col"><p>Granito</p></div>
            <div class="col"><p>Limestone</p></div>
            <div class="col"><p>Nanoglass</p></div>
            <div class="col"><p>Quartzo</p></div>
            <div class="col"><p>Quartzito</p></div>
            <div class="col"><p>Silestone</p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Cozinha</p></div>
            <div class="col"><p></p></div>
            <div class="col"><p></p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p></p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p></p></div>
            <div class="col"><p>*</p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Banheiro</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Piso interno e soleiras</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>o</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Piso externo</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p></p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>o</p></div>
            <div class="col"><p></p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Parede interna</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
        </div>
        <div class="linha-tab">
            <div class="col title"><p>Parede externa</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>*</p></div>
            <div class="col"><p>o</p></div>
            <div class="col"><p>o</p></div>
            <div class="col"><p>o</p></div>
            <div class="col"><p></p></div>
        </div>
    </div>
    <div class="legenda">
        <p>* RECOMENDÁVEL</p>
        <p>O EM ALGUNS CASOS</p>
    </div>

</div> <!-- Box Serviços -->

</div> <!-- Box Serviços Total -->

	</div> <!-- Conteudo-pages -->
</div>