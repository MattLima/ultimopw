<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 12";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 12\" />";
    $dadosPagina["css"] = "<style></style>";
?>


<div class="conteudo-pages">


	<div class="titulo">
		<h1><i class="fas fa-toolbox"></i> <br> ENTRE EM CONTATO</h1>
	</div>
	<div class="contato-total">

		<div class="contato">

			<div class="info-total">
				<div class="infos">
					<div class="titulo-contato">
						Contato <div class="linha"></div>
					</div>

					<div class="conteudo">
						<p><span><i class="fas fa-map-marker-alt"></i></span> Av. Dourado, 233 - Sala B <br> Res. Aquário - Vinhedo/SP</p>
						<p><span><i class="fas fa-phone"></i></span> (19) 3826-2294</p>
						<p><span><i class="fas fa-envelope"></i></span> comercial@projetowebsite.com.br</p>
					</div>
				</div>
			</div>

			<div class="formulario-total">
				<div class="formulario">
				<div class="titulo-form"><h2>FALE CONOSCO</h2></div>
					<form action="mail-contato.php" method="post">

						<input name="campo[Nome]" placeholder="Nome:" type="text" />
						<input name="campo[E-mail]" placeholder="E-mail:" type="text" />
						<input name="campo[Telefone]" placeholder="Telefone:" type="text" />
						<textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
						<input class="submit" value="Enviar" type="submit" />

					</form>
				</div>
			</div>
		</div>

	</div>
</div> <!-- conteudo pages -->
