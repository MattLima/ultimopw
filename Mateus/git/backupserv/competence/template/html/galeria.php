<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>BIBLIOTECA DE FOTOS</h1>
</div>

<div class="page">
    <div class="row single">

        <div class="gallery">
            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/neily-e-crianças.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/neily-e-crianças.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/Bete e alunos.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/Bete e alunos.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/Concita e alunos.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/Concita e alunos.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_2820.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_2820.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_3868.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_3868.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_3886.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_3886.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_3895.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_3895.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_3915.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_3915.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_3925.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_3925.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7580.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7580.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7606.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7606.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7692.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7692.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7713.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7713.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7724.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7724.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7730.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7730.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/IMG_7740.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/IMG_7740.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/P3020007.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/P3020007.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/P3020008.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/P3020008.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/PEROLLA gorro.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/PEROLLA gorro.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/Quezia - A Julia.jpg" title="">
                    <img src="[template]/pw-images/galeria/thumb/Quezia - A Julia.jpg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/Rodrigo e Geovana.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/Rodrigo e Geovana.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC10966.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC10966.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC11031.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC11031.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC11119.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC11119.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC11259.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC11259.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC11671.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC11671.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC11723.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC11723.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12068.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12068.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12085.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12085.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12088.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12088.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12096.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12096.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12123.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12123.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12729.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12729.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/SDC12747.JPG" title="">
                    <img src="[template]/pw-images/galeria/thumb/SDC12747.JPG" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-08-19 at 7.04.18 AM (1).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-08-19 at 7.04.18 AM (1).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-08-19 at 7.04.18 AM (2).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-08-19 at 7.04.18 AM (2).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-08-28 at 12.57.31 PM.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-08-28 at 12.57.31 PM.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-09-24 at 10.08.07 AM.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-09-24 at 10.08.07 AM.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-10-23 at 11.19.27 (1).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-10-23 at 11.19.27 (1).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-10-23 at 11.19.28 (1).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-10-23 at 11.19.28 (1).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-10-23 at 11.19.28 (2).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-10-23 at 11.19.28 (2).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2019-11-20 at 08.49.48.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2019-11-20 at 08.49.48.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2020-01-09 at 08.14.11 (1).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2020-01-09 at 08.14.11 (1).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-02-25 at 10.21.03 (1).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-02-25 at 10.21.03 (1).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-02-25 at 10.21.03 (2).jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-02-25 at 10.21.03 (2).jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-03-11 at 15.32.49.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-03-11 at 15.32.49.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-03-11 at 15.33.26.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-03-11 at 15.33.26.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-03-22 at 18.06.31.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-03-22 at 18.06.31.jpeg" alt="" title="">
                </a>
            </div>

            <div class="foto-gallery">
                <a href="[template]/pw-images/galeria/WhatsApp Image 2021-03-22 at 18.09.43.jpeg" title="">
                    <img src="[template]/pw-images/galeria/thumb/WhatsApp Image 2021-03-22 at 18.09.43.jpeg" alt="" title="">
                </a>
            </div>
        </div>

    </div>
</div>