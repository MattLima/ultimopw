<?php
$dadosPagina["titulo"]   = "Parcerias Network";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Parcerias, Cowork, Network, BluesSky, Virtua, CitiusLog, Networking, Coworking, Espaço Coworking.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Parcerias Network – All Flags\" />";
$dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">

    <div class="titulo">
        <h1><i class="far fa-building"></i> <br> Parceiros</h1>
    </div>

    <div class="box-conteudo">

        <div class="galeria">
            <div class="itemGaleria">
                <a target="_blank" href="#"> <img src="[template]/pw-images/parceiros/bluesky.jpg" alt="BlueSky Negócios " title="BluesSky Negócios"></a>
            </div>
            <div class="itemGaleria">
                <a target="_blank" href="https://citiuslog.com/"><img src="[template]/pw-images/parceiros/citius-log.jpg" alt="CitiusLog" title="CitiusLog"></a>
            </div>
            <div class="itemGaleria">
                <a target="_blank" href="http://virtuagestaocomercial.com/site/"><img src="[template]/pw-images/parceiros/virtua.jpg" alt="Virtua" title="Virtua"></a>
            </div>
        </div>

    </div>

</div>