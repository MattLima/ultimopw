<?php
$dadosPagina["titulo"]   = "Coworking – All Flags";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Coworking, Networking, Empresa Coworking, Empresa Networking, Escritório compartilhado, Espaço de Coworking.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Coworking – All Flags\" />";
$dadosPagina["css"] = "<style></style>";
?>
<div class="swiper-container">
	<div class="swiper-wrapper">
		
		<div class="swiper-slide"><img src="[template]/pw-images/banners/bandeiras-coworking.jpg" alt="Bandeiras de coworking" title="Bandeiras de coworking" /></div>
		<div class="swiper-slide"><img src="[template]/pw-images/banners/consultoria-coworking.jpg" alt="Consultoria Coworking" title="Consultoria Coworking" /></div>
		<div class="swiper-slide"><img src="[template]/pw-images/banners/parcerias-coworking.jpg" alt="Parcerias coworking" title="Parcerias coworking" /></div>
		<div class="swiper-slide"><img src="[template]/pw-images/banners/sala-coworking.jpg" alt="Sala coworking" title="Sala coworking" /></div>

	</div>
	<!-- Add Arrows -->
	<div class="swiper-button-next swiper-button-white"></div>
	<div class="swiper-button-prev swiper-button-white"></div>


	<div class="slogan-total">
	<div class="slogan">
		<div class="titulo">Traga a sua Bandeira para AllFlags coworking e aumente sua esfera de contactos!</div>
		<div class="texto"> <b>Você tem a sua Bandeira, nós temos todas </b></div>
	</div>
	</div>

</div>



<div class="box-01-total">

	<div class="box-01">

		<div class="titulo">Torne-se parceiro e descubra uma nova forma de ampliar sua rede de contactos</div>

	</div>

</div>

<div class="box-02-total">

	<div class="titulo">CONHEÇA NOSSO ESPAÇO NEGÓCIOS</div>

	<div class="box-02">

		<div class="item">

			<div class="img"><img src="[template]/pw-images/sobre-coworking.jpg" alt="Sobre de Coworking" title="Sobre  Coworking" ></div>

			<div class="text">
				<div class="titulo">SOBRE NÓS</div>
				<div class="texto"><a href="[url]/sala-networking-coworking" title="Sobre  Coworking">Leia Mais</a></div>
			</div>

		</div>

		<div class="item">
			<div class="img"><img src="[template]/pw-images/serviços-coworking.jpg" alt="Serviços de Coworking" title="Serviços de Coworking" ></div>
			<div class="text">
				<div class="titulo">SERVIÇOS</div>
				<div class="texto"><a href="[url]/sala-reuniao" title="Serviços de Coworking">Leia Mais</a></div>
			</div>
		</div>

		<div class="item">
			<div class="img"><img src="[template]/pw-images/pacote-coworking.jpg" alt="Pacotes de Coworking" title="Pacotes de Coworking" ></div>
			<div class="text">
				<div class="titulo">PACOTE A SUA MEDIDA</div>
				<div class="texto"><a href="[url]/pacote-a-sua-medida" title="Pacotes de Coworking">Leia Mais</a></div>
			</div>
		</div>

	</div>

</div>

<div class="box-03-total">

	<div class="box-03">

		<div class="titulo">NOSSOS PACOTES</div>

		<div class="conteudo">

			<div class="item">
				<div class="numero">Bandeira 1</div>
				<div class="texto">
					<ul>
						<li><i class="fas fa-check"></i> Estação de trabalho rotativo</li>
						<li><i class="fas fa-check"></i> Wi-Fi de alta velocidade</li>
						<li><i class="fas fa-check"></i> Receção de Correspondência</li>
					</ul>
				</div>
			</div>

			<div class="item">
				<div class="numero">Bandeira 2</div>
				<div class="texto">
					<ul>
						<li><i class="fas fa-check"></i> Estação de Trabalho fixo </li>
						<li><i class="fas fa-check"></i> Bloco de gavetas</li>
						<li><i class="fas fa-check"></i> Internet Wi-Fi </li>
						<li><i class="fas fa-check"></i> Domiciliação Fiscal e Comercial</li>
						<li><i class="fas fa-check"></i> Receção de Correspondência</li>
					</ul>
				</div>
			</div>

			<div class="item">
				<div class="numero">Bandeira 3</div>
				<div class="texto">
					<ul>
						<li><i class="fas fa-check"></i> Estação de Trabalho fixo individual </li>
						<li><i class="fas fa-check"></i> Bloco de gavetas</li>
						<li><i class="fas fa-check"></i> Internet Wi-Fi </li>
						<li><i class="fas fa-check"></i> Número de telefone individual</li>
						<li><i class="fas fa-check"></i> Domiciliação Fiscal e Comercial</li>
						<li><i class="fas fa-check"></i> Receção de Correspondência</li>
					</ul>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="box-04-total">

	<div class="box-04">

		<div class="titulo">CONTACTO</div>

		<form action="pw-form.php" method="post">

			<div class="cima">
				<input name="campo[Nome]" placeholder="Nome:" type="text" />
				<input name="campo[E-mail]" placeholder="E-mail:" type="text">
				<input name="campo[Telefone]" placeholder="Telefone:" type="text">
			</div>
			
			<textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>

			<span><input class="submit" value="Enviar" type="submit" title="Enviar formulário"/></span>

		</form>

	</div>

</div>

<div class="mapa">


	<div class="mapa-img">
		<a href="https://goo.gl/maps/ddMLvqGSEhQwKKYP6"> <img src="[template]/pw-images/MapaAllFlags.jpg" alt="AllFlux Localização" title="AllFlux Localização"> </a> 
	</div>


	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1501.300228757663!2d-8.655934041740505!3d41.186880913360156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2465f9f4696fb9%3A0xcaa611f7360ea54a!2sAv.%20Fabril%20do%20Norte%20819%201%C2%BA.%20Andar%2C%204460-317%20Sra.%20da%20Hora%2C%20Portugal!5e0!3m2!1spt-BR!2sbr!4v1616155191512!5m2!1spt-BR!2sbr"  height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>