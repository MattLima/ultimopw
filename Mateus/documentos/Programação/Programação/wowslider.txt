  <div id="wowslider-container1">
        <div class="ws_images">
            <?php $fields = get_fields(10);?>

            <ul>
                <?php foreach($fields as $field){?>
                <?php if($field['url'] != ""){?>
                <li><img src="<?php echo $field['url'];?>" alt="<?php echo $field['alt'];?>" title="<?php echo $field['alt'];?>" /></li>
                <?php }?>
                <?php }//FECHA FOREACH $fields?>
            </ul>
        </div>

        <div class="ws_shadow"></div>
    </div>
    
    