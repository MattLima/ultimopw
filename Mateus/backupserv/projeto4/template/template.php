<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

        <title>[pagina]</title>

        <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

        

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="format-detection" content="telephone=no" />

        <style><?php echo file_get_contents('template/pw-css/style.css');?></style>
        <style><?php echo file_get_contents('template/pw-slider-engine/style.css');?></style>
        <style><?php echo file_get_contents('pw-font-awesome/css/all.css');?></style>

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100&display=swap" rel="stylesheet">
    
    [css]
    
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>
</head>

<body>

	<div class="topo-total">


        <div class="topo-contato-total">

            <div class="topo-contato">

                <div class="conteudo-contato">
                    <i class="fas fa-phone"></i>
                    <p>+55 11 2376-9961 / 2376-9965</p>
                </div>

                <div class="conteudo-contato">
                    <i class="fab fa-whatsapp"></i>
                    <p>+55 11 98987-9151 / 98983-5189</p>
                </div>

                <div class="conteudo-contato">
                    <i class="far fa-envelope"></i>
                    <p>comercial@afluxbrasil.com.br</p>
                </div>

            </div>
                
        </div>
    
    	<div class="topo">

        	<div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logoAflux.png" alt="" title="" /></a></div> <!-- Logo -->

            <div class="menu-total">
                <ul>
                    <li>HOME</li>
                    <li>EMPRESA</li>
                    <li>PRODUTOS</li>
                    <li>SERVIÇOS</li>
                    <li>CLIENTES</li>
                    <li>CONTATO</li>
                </ul>
            </div>
        </div> <!-- Topo -->
    
    </div> <!-- Topo Total -->
    


<div class="global">

	[conteudo]
	
</div> <!-- global -->

	<div class="rodape-total">
    
    	<div class="rodape">


            <div class="rodape-logo">
                <img src="[template]/pw-images/logo-Branco.png" alt="" title="" />
                <p>AFLUX BRASIL LTDA</p>
            </div>

            <div class="rodape-contato">

                <div class="rodape-contato-conteudo">
                    <i class="fas fa-phone"></i>
                    <p>Telefone <br> +55 11 2376-9961 </p>
                </div>

                <div class="rodape-contato-conteudo">
                    <i class="fas fa-mobile-alt"></i>
                    <p>Celular: <br> +55 11 98987-9151 <br> +55 11 98983-5189 </p>
                </div>

                <div class="rodape-contato-conteudo">
                    <i class="far fa-envelope"></i>
                    <p>Emails: <br> comercial@afluxbrasil.com.br </p>
                </div>

                <div class="rodape-contato-conteudo">
                    <i class="fas fa-map-marker-alt"></i>
                    <p>Endereço <br> Rua Lisboa, 41 - Osvaldo Cruz <br> CEP: 09570-510 - São Caetano do Sul - SP </p>
                </div>
                

            </div>

            <div class="rodape-ProjetoWeb">
            
            <img src="[template]/pw-images/logo-PW-Branco.png" alt="" title="" />

            </div>

        
        </div> <!-- Rodape -->
    
    </div> <!-- Rodape Total -->
	
        <script><?php echo file_get_contents('pw-js/jquery.js');?></script>
        <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
        <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
        <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
        <script>
        
            (function($) {	
                'use strict';
                window.sr = ScrollReveal();
                sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
            })();
            
            
        </script>
</body>

</html>